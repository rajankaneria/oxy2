<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    public function __construct(){
    	parent::__construct();
        if(!$this->session->userdata('fullname')){
			header("Location: ".base_url());
			exit();
		}
    }
	public function index()
	{
        $this->load->model("user_model");
        $current_user = $this->user_model->currentUser();
		$headerData = array(
			"pageTitle" => "Profile",
			"breadcrumb" => array(base_url()."dashboard"=>"Dashboard",base_url()."user/"=>"User Profile")
		);
		$footerData = array(
			"jsFiles" => array("profile.js")
		);
		$viewData = array(
			"viewName" => "profile",
            "viewData" => $current_user,
			"headerData" => $headerData,
			"footerData" => $footerData
		);
		$this->load->view('template',$viewData);
	}	

	public function updateUser()
	{
		$this->load->model("user_model");
		$userData = $this->input->post("userData");
		$this->user_model->update($userData);
		$data = array(
			"fullname" =>$userData["first_name"]." ".$userData["last_name"]
		);
		$this->session->set_userdata($data);

	}

	public function jobfunction()
	{
		$this->load->model("user_model");
        $userRow = $this->user_model->currentUser();
        if($userRow["s_job_function_id"]<6)
        {
          	header("Location: ".base_url()."assignments");
        	exit();
        }
        else
        {
        	$headerData = array(
				"pageTitle" => "Profile",
				"breadcrumb" => array(base_url()."dashboard"=>"Dashboard",base_url()."user/jobfunction"=>"Job Function")
			);
			$footerData = array(
				"jsFiles" => array("job_function.js")
			);
			$viewData = array(
				"viewName" => "job_function",
	            "viewData" => $userRow,
				"headerData" => $headerData,
				"footerData" => $footerData
			);
			$this->load->view('template',$viewData);
        }
	}
	public function getUserList()
    {

      	$this->load->model("user_model");
      	$userArray = $this->user_model->getAllUsers();
    	echo json_encode($userArray);
    }
    public function saveJobFunction()
    {
    	$this->load->model("user_model");
    	$userID = $this->input->post("user");
    	$jobFunction = $this->input->post("jobfunction");
    	$this->user_model->updateJobFunction($userID,$jobFunction);
    }

    public function getCurrentUser()
    {
    	$this->load->model("user_model");
        $userRow = $this->user_model->currentUser();
        echo json_encode($userRow);
    }
    public function updateUserField()
    {
    	//$this->db->query("update s_users set field_collection='1',arcflash='1',gis='1' where id='1'");
        //$this->db->query("ALTER TABLE s_users ADD COLUMN notify character varying(50) NOT NULL DEFAULT 'Yes'");
    }

    public function sendReportNotification()
    {
    	$savedFields = $_POST["savedFields"];
    	$output = "";
    	foreach ($savedFields as $substationID => $savedFieldArray) {
    		$savedFieldArray = array_unique($savedFieldArray, SORT_REGULAR);
    		$substationHtml = $this->substationEmailHtml($savedFieldArray);
    		$output = $output.$substationHtml;
    	}
    	$this->load->model("user_model");
        $userRow = $this->user_model->currentUser();
        if($userRow["notify"]=="Yes")
        {
            $subject = "Progress Report Update";
            $email = $userRow["email"];
            $this->sendEmail($subject,$output,$email);
        }

    }

    public function substationEmailHtml($savedFieldArray)
    {
    	$substationName = $savedFieldArray[0]["substationName"];
    	$fieldArray = array();
    	foreach ($savedFieldArray as $key => $value) {
    		$columnArray = explode("_",$value["column"]);
    		$fieldName = implode("_", array_slice($columnArray, 0, sizeof($columnArray)-1));
    		$fieldType = $columnArray[sizeof($columnArray)-1];
    		$fieldArray[$fieldName][$fieldType] =  $value["value"];
    	}

    	$viewData = array(
    		"substationName" => $substationName,
    		"fieldArray" => $fieldArray
    	);
    	return $this->load->view("report_notification_row",$viewData,TRUE);
    }

    public function sendEmail($subject,$body,$email)
    {

		$this->load->library('email');
		$this->email->from('citygatecollector@gmail.com'); // change it to yours
		$this->email->to($email);// change it to yours
		$this->email->subject($subject);
		$this->email->message($body);
		if($this->email->send())
		{
            echo 'Email sent.';
		}
		else
		{
		  echo $this->email->print_debugger();
		}
    }
    public function emailTest()
    {
        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->from('citygatecollector@gmail.com'); // change it to yours
        $this->email->to("rajan.kaneria@gmail.com");// change it to yours
        $this->email->subject("test");
        $this->email->message("Test message");
    }

}
