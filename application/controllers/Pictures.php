<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pictures extends CI_Controller {

    public function __construct(){
    	parent::__construct();
        if(!$this->session->userdata('fullname')){
    			header("Location: ".base_url());
    			exit();
    		}
        else
        {
          $this->load->model("user_model");
          $userRow = $this->user_model->currentUser();
          if($userRow["s_job_function_id"]<5)
          {
            header("Location: ".base_url()."assignments");
            exit();
          }
        }
    }

    public function review()
    {
      $this->load->model("user_model");
      $userList = $this->user_model->getAllUsers();
      $currentUser = $this->user_model->currentUser();
    	$headerData = array(
  			"pageTitle" => "Review Pictures",
  			"breadcrumb" => array(base_url()."dashboard"=>"Dashboard",base_url()."assignments"=>"Assignments",base_url()."pictures/review"=>"Review Pictures")
  		);
  		$footerData = array(
  			"jsFiles" => array("jquery.mCustomScrollbar.js","jquery.slimscroll.js","review_pictures.js")
  		);
  		$viewData = array(
  			"viewName" => "review_pictures",
  			"viewData" => array("userList"=>$userList,"currentUser"=>$currentUser),
  			"headerData" => $headerData,
  			"footerData" => $footerData
  		);
  		$this->load->view('template',$viewData);
    }

    public function getPictureList()
    {
      $this->load->model("picture_model");
      $this->load->model("user_model");
      $currentUser = $this->user_model->currentUser();
      $pictureArray = $this->picture_model->getAllPictures($currentUser);

      foreach ($pictureArray as $key => $pictureRow) {
        $pictureArray[$key]["reviewerName"] = "";
        if($pictureRow["email"]!="" && $pictureRow["first_name"]!="" && $pictureRow["last_name"]!="")
        {
          $pictureArray[$key]["reviewername"] = $pictureRow["first_name"]." ".$pictureRow["last_name"]." (".$pictureRow["email"].")";
        }
        elseif($pictureRow["email"]!="" && $pictureRow["first_name"]=="" && $pictureRow["last_name"]=="")
        {
          $pictureArray[$key]["reviewerName"] = $pictureRow["email"];
        }
        
      }

    	echo json_encode($pictureArray);
    }
    public function rotateImage()
    {
        ini_set('display_errors', 0);
        ini_set('memory_limit', '256M');
        $this->load->model("picture_model");
        $this->load->model("user_model");
        $current_user = $this->user_model->currentUser();
        $fid = $this->input->post("fid");
        $rotation = $this->input->post("rotation");

        $imageRow = $this->picture_model->getPictureDetails($fid);
        $filePath = $imageRow["name"];
        $pathArray = explode("/", $filePath);
        $fileName = $pathArray[sizeof($pathArray) - 1];
        
        $hostName = $_SERVER["HTTP_HOST"];
        $webRoot = $_SERVER['DOCUMENT_ROOT'];
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
        $savePath = str_replace($protocol.$hostName, "", $filePath);
        $savePath = $webRoot.$savePath;


        if($rotation!=0)
        {
            $root = getcwd();
            // File and rotation
            $imageName = $fileName;
            $degrees = $rotation;
            $id = $fid;
            $rotateFilename = $filePath; // PATH

            if($degrees!=180 && $degrees!=0)
            {
                $degrees = $degrees + 180;
            }

            $fileType = strtolower(substr($imageName, strrpos($imageName, '.') + 1));
            $imageSrc = $rotateFilename;
            list($width, $height) = getimagesize($imageSrc);
            $percent = 1;
            $new_width = $width * $percent;
            $new_height = $height * $percent;
            $image_p = imagecreatetruecolor($new_width, $new_height);
            
            
            if($fileType == 'png' || $fileType == 'PNG'){
               header('Content-type: image/png');
               $source = imagecreatefrompng($rotateFilename);
               imagecopyresampled($image_p, $source, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
               $bgColor = imagecolorallocatealpha($source, 255, 255, 255, 127);
               // Rotate
               $rotate = imagerotate($image_p, $degrees, $bgColor);
               imagesavealpha($rotate, true);
               imagepng($rotate,$savePath);
            
            }
            
            if($fileType == 'jpg' || $fileType == 'jpeg'){
               header('Content-type: image/jpeg');
               $source = imagecreatefromjpeg($rotateFilename);
               imagecopyresampled($image_p, $source, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
               // Rotate
               $rotate = imagerotate($image_p, $degrees, 0);
               imagejpeg($rotate,$savePath);
            }
            // Free the memory
            imagedestroy($source);
            imagedestroy($rotate);
            imagedestroy($image_p);
        }

        $date = new DateTime("now", new DateTimeZone('America/New_York') );
        $imageData = array(
          "rotation" => $rotation,
          "reviewedby" => $current_user["id"],
          "reviewdate" => $date->format('Y-m-d'),
          "reviewdatetime" => $date->format('Y-m-d H:i:s')
        );
        $this->picture_model->updateImageData($imageData,$fid);
        echo json_encode($imageData);
    }

    public function saveImage()
    {
        $root = getcwd();
        $this->load->model("picture_model");
        // File and rotation
        $imageSrc = $_POST["imageSrc"];
        $api_no = $_POST["api_no"];
        $fileNameArray = explode("/", $imageSrc);
        $imageName = $fileNameArray[sizeof($fileNameArray)-1];
        
        $fileType = strtolower(substr($imageName, strrpos($imageName, '.') + 1));
        $newImageName = time()."_".$api_no.".".$fileType;
        $savePath = $root."/html/assets/pictures/".$newImageName;

        list($width, $height) = getimagesize($imageSrc);
        $percent = 1;
        if($width > 2000 || $height > 2000)
        {
          $percent = 0.5;
        }
        $new_width = $width * $percent;
        $new_height = $height * $percent;
        $image_p = imagecreatetruecolor($new_width, $new_height);
        if($fileType == 'png' || $fileType == 'PNG'){
           header('Content-type: image/png');
           $source = imagecreatefrompng($imageSrc);
           imagecopyresampled($image_p, $source, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
           imagepng($image_p,$savePath);

        }

        if($fileType == 'jpg' || $fileType == 'jpeg'){
           header('Content-type: image/jpeg');
           $source = imagecreatefromjpeg($imageSrc);
           imagecopyresampled($image_p, $source, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
           imagejpeg($image_p,$savePath);
        }
        $this->picture_model->savePicture($api_no,$newImageName);
        // Free the memory
        imagedestroy($source);
        imagedestroy($image_p);
        
    }
    public function assignUser()
    {
        $this->load->model("picture_model");
        $this->load->model("user_model");
        $fid = $this->input->post("fid");
        $userID = $this->input->post("userID");
        $user_row = $this->user_model->userData($userID);

        $reviewerName = "";
        if($user_row["email"]!="" && $user_row["first_name"]!="" && $user_row["last_name"]!="")
        {
          $reviewerName = $user_row["first_name"]." ".$user_row["last_name"]." (".$user_row["email"].")";
        }
        elseif($user_row["email"]!="" && $user_row["first_name"]=="" && $user_row["last_name"]=="")
        {
          $reviewerName = $user_row["email"];
        }

        $this->picture_model->updateImageData(array("reviewedby" => $userID),$fid);
        $imageData = array(
          "reviewedby" => $userID,
          "reviewername" => $reviewerName
        );
        echo json_encode($imageData);
    }

}