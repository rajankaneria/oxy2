<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Database extends CI_Controller {
	public function __construct(){
    	parent::__construct();
        if(!$this->session->userdata('fullname')){
			header("Location: ".base_url());
			exit();
		}
    }

	public function index()
	{
		$this->load->model("assignment_model");
		$substationList = $this->assignment_model->getSubstationList();
		$headerData = array(
			"pageTitle" => "Database",
			"breadcrumb" => array(base_url()."dashboard"=>"Dashboard",base_url()."database/"=>"Database")
		);
		$footerData = array(
			"jsFiles" => array("jquery.geo-1.0.0-rc1.1.min.js","common.js","view_database.js","jquery-qrcode-0.14.0.min.js")
		);
		$viewData = array(
			"viewName" => "view_database",
			"viewData" => array("substationList"=>$substationList),
			"headerData" => $headerData,
			"footerData" => $footerData
		);
		$this->load->view('template',$viewData);
	}



	function tables()
	{
		$this->load->helper('url');
		$parameters = $this->uri->uri_to_assoc();
		if(isset($parameters["name"]))
		{
			$table_name = $parameters["name"];
			$this->load->model("database_model");
			$field_list = $this->database_model->field_list($table_name);
			foreach($field_list as $field)
			{
				echo "<b>".$field->name."</b>";
				var_dump($field);
				echo "<br/>";
			}			
		}
		else 
		{
			$this->load->model("database_model");
			$table_list = $this->database_model->table_list();
			foreach($table_list as $table)
			{
				echo "<div><a href='".site_url('database/table_data/name/'.$table)."'>".$table."</a></div>";
			}
		}
	}
	function table_data()
	{
		$this->load->helper('url');
		$parameters = $this->uri->uri_to_assoc();
		if(isset($parameters["name"]))
		{
			$table_name = $parameters["name"];
			$this->load->model("database_model");
			$this->database_model->table_data($table_name);			
		}
		else 
		{
			$this->load->model("database_model");
			$table_list = $this->database_model->table_list();
			foreach($table_list as $table)
			{
				echo "<div><a href='".site_url('database/table_data/name/'.$table)."'>".$table."</a></div>";
			}
		}
	}

	function motorDetails($api_no)
	{
		$this->load->model("database_model");
		//$features = $this->database_model->all_features();
		$features = array(
			"controlpanel",
			"overcurrentdevicebank",
			"tappoint",
			"transformerbank"
		);
		foreach ($features as $key => $featureData) {
			//$table_name = $featureData["postgresname"];
			$table_name = $featureData;
			$feature_data = $this->database_model->feature_data($api_no,$table_name);
			if($feature_data!=null && sizeof($feature_data)!=0)
			{
				$output[$table_name] = $feature_data;
			}
			
		}
		echo json_encode($output);
	}
	function motorDiagramTest($api_no)
	{
		$this->load->view('diagramTest',array("api_no" => $api_no));
	}

	function motorDiagram($api_no)
	{
		$this->load->model("database_model");
		$features = $this->database_model->all_features();
		// features should be in the order of connecting lines
		/*
		$features = array(
			"controlpanel" => 1,
			"transformerbank" => 3,
			"overcurrentdevicebank" => 3,
			"tappoint" => 3
		);
		*/
		foreach ($features as $key => $featureData) {
			$table_name = $featureData["postgresname"];
			$feature_data = $this->database_model->feature_data($api_no,$table_name);
			if($feature_data!=null && sizeof($feature_data)!=0)
			{
				$output[$table_name] = array(
					"x" => $feature_data["x"],
					"y" => $feature_data["y"],
					"label" => $featureData["name"],
					"id" => $feature_data["ogc_fid"]
				);
			}
			
		}
		$x_max_min = $this->max_min("x",$output);
		$y_max_min = $this->max_min("y",$output);
		$canvasHeight = "500";
		foreach ($output as $index => $value) {
			$newPoints[$index]["x"] = $this->convertUnit($x_max_min,$canvasHeight,$value["x"]);
			$newPoints[$index]["y"] = $this->convertUnit($y_max_min,$canvasHeight,$value["y"]);
			$newPoints[$index]["label"] = ucwords($value["label"]);
			$newPoints[$index]["id"] = $value["id"];
			//$newPoints[$index] = $this->scalePoint($newPoints[$index],1);
			//$newPoints[$index]["distanceFromOrigin"] = sqrt(pow($newPoints[$index]["x"],2) + pow($newPoints[$index]["y"],2));
		}
		//var_dump($newPoints);
		echo json_encode($newPoints);
	}

	function scalePoint($point,$scale)
	{
		$newPoint["x"] = $point["x"]*$scale;
		$newPoint["y"] = $point["y"]*$scale;
		return $newPoint;
	}

	function convertUnit($max_min_array,$new_max,$input)
	{
		$oldUnitDifference = $max_min_array["max"] - $max_min_array["min"];
		$scalingRatio = $oldUnitDifference/$new_max;
		$inputDifference = $input - $max_min_array["min"];
		$output = $inputDifference/$scalingRatio;
		return $output;
	}

	function max_min($key,$array)
	{
		$valueArray = array();
		foreach ($array as $index => $value) {
			$valueArray[] = $value[$key];
		}
		$output = array(
			"max" => max($valueArray),
			"min" => min($valueArray)
		);
		return $output;
	}

	function getAllFeatures()
	{
		$this->load->model("database_model");
		$features = $this->database_model->all_features();
		foreach ($features as $key => $feature_data) {
			echo $feature_data["postgresname"]."<br/>";
		}
	}

	function getMotorEdges($api_alphanumber)
	{
		$this->load->model("database_model");
		$lines = $this->database_model->associated_lines($api_alphanumber);
		$nodeList = $this->getNodeList($lines);
		$output = array(
			"nodeList" => $nodeList,
			"lines" => $lines
		);
		echo json_encode($output);
	}
	 function getNodeList($lines)
	 {
	 	$this->load->model("database_model");
	 	$output = array();
	 	foreach ($lines as $key => $lineGroup) {
		 	foreach ($lineGroup as $index => $lineArray) {
		 		$fromNode = array($lineArray["fromguid"] => $lineArray["fromlayer"]);
		 		$output = array_merge($output,$fromNode);
		 		$toNode = array($lineArray["toguid"] => $lineArray["tolayer"]);
		 		$output = array_merge($output,$toNode);
	 		}

	 	}
	 	foreach ($output as $guid => $layerName) {
	 		$tableName = $this->database_model->getTableName($layerName);
	 		$output[$guid] = array(
	 			"tableName" => $tableName,
	 			"label" => $layerName
	 		);
	 	}
	 	return $output;
	 }

	 function getFeatureDetails()
	 {
	 	$this->load->model("database_model");
	 	$featureName = strtolower($_POST["featureName"]);
	 	$featureID = $_POST["featureId"];
	 	$output = $this->database_model->getFeature($featureName,$featureID);
	 	echo json_encode($output);

	 }

	 function getLineDetails()
	 {
	 	$this->load->model("database_model");
	 	$tableName = $_POST["lineType"]."line";
	 	$to = $_POST["to"];
	 	$from = $_POST["from"];
	 	$output = $this->database_model->getLine($tableName,$to,$from);
	 	echo json_encode($output);
	 }
	
	function checkMotor()
	{
		$query = $this->db->query("select * from motor where api_alphanumber!=''");
		$result = $query->result_array();
		var_dump($result);
	}

	function feature_fields()
	{
		$this->load->view('feature_fields');
	}
	function insert_feature_field()
	{
		$table_name = $_POST["table_name"];
		$field_name = $_POST["field_name"];
		$field_label = $_POST["field_label"];
		$is_link = $_POST["is_link"];

		$this->db->query("insert into s_feature_fields(table_name,field_name,field_label,is_link) values('$table_name','$field_name','$field_label','$is_link')");
	}

	function getSubstationList()
	{
		$this->load->model("database_model");
		$substationList = $this->database_model->getSubstationList();
		echo json_encode($substationList);
	}

	function saveSubstationField()
	{
		$this->load->model("database_model");
		$substationID = $_POST["substationID"];
		$column = $_POST["column"];
		$value = $_POST["value"];
		$this->database_model->saveSubstationField($substationID,$column,$value);
	}
}
