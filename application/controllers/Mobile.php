<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile extends CI_Controller {
	public function __construct(){
    	parent::__construct();
    }

    public function motordiagram($api_no)
    {
    	$this->load->model("assignment_model");
		$headerData = array(
			"pageTitle" => "Motor Diagram"
		);
		$footerData = array(
			"jsFiles" => array("mobile_motordiagram.js")
		);
		$viewData = array(
			"viewName" => "mobile_motordiagram",
			"viewData" => array("api_no"=>$api_no),
			"headerData" => $headerData,
			"footerData" => $footerData
		);
		$this->load->view('mobile_template',$viewData);
    }
    public function mapview()
    {
    	$api_no = $_GET["api_no"];
    	$this->load->model("assignment_model");
    	$substationList = $this->assignment_model->getSubstationList();
    	$substationID = $this->assignment_model->motorSubstation($api_no);
		$motorLocation = $this->assignment_model->motorLocation($api_no);
		$headerData = array(
			"pageTitle" => "Map View"
		);
		$footerData = array(
			"jsFiles" => array("jquery.geo-1.0.0-rc1.1.min.js","mobile_mapview.js")
		);
		$viewData = array(
			"viewName" => "mobile_mapview",
			"viewData" => array(
				"substationList"=>$substationList,
				"selectedSubstation"=>$substationID,
				"motorX"=>$motorLocation["x"],
				"motorY"=>$motorLocation["y"]
			),
			"headerData" => $headerData,
			"footerData" => $footerData
		);
		$this->load->view('mobile_template',$viewData);
    }

    function getMotorEdges($api_alphanumber)
	{
		$this->load->model("database_model");
		$lines = $this->database_model->associated_lines($api_alphanumber);
		$nodeList = $this->getNodeList($lines);
		$output = array(
			"nodeList" => $nodeList,
			"lines" => $lines
		);
		echo json_encode($output);
	}
	function getNodeList($lines)
	{
	 	$this->load->model("database_model");
	 	$output = array();
	 	foreach ($lines as $key => $lineGroup) {
		 	foreach ($lineGroup as $index => $lineArray) {
		 		$fromNode = array($lineArray["fromguid"] => $lineArray["fromlayer"]);
		 		$output = array_merge($output,$fromNode);
		 		$toNode = array($lineArray["toguid"] => $lineArray["tolayer"]);
		 		$output = array_merge($output,$toNode);
	 		}

	 	}
	 	foreach ($output as $guid => $layerName) {
	 		$tableName = $this->database_model->getTableName($layerName);
	 		$output[$guid] = array(
	 			"tableName" => $tableName,
	 			"label" => $layerName
	 		);
	 	}
	 	return $output;
	}


	function getFeatureDetails()
	{
		$this->load->model("database_model");
	 	$featureName = strtolower($_POST["featureName"]);
	 	$featureID = $_POST["featureId"];
	 	$output = $this->database_model->getFeature($featureName,$featureID);
	 	echo json_encode($output);

	}

	function getLineDetails()
	{
	 	$this->load->model("database_model");
	 	$tableName = $_POST["lineType"]."line";
	 	$to = $_POST["to"];
	 	$from = $_POST["from"];
	 	$output = $this->database_model->getLine($tableName,$to,$from);
	 	echo json_encode($output);
	}

	public function getSubstationGeom($substationID)
	{
			$this->load->model("assignment_model");
			$substationGeom = $this->assignment_model->getSubstationGeom($substationID);
			echo json_encode($substationGeom,true);		
	}

	public function getSubstationData($substationID)
	{
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getSubstationRow($substationID);
		echo json_encode($result);
	}

	public function getMotorData($ogc_fid)
	{
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getMotorRow($ogc_fid);
		echo json_encode($result);
	}

	public function getPoleData($ogc_fid)
	{
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getPoleRow($ogc_fid);
		echo json_encode($result);
	}

	public function getFedderColors()
	{
		$colorArray = array("#ef5350","#ab47bc","#7e57c2","#5c6bc0","#42a5f5","#26c6da","#26c6da","#d4e157","#66bb6a","#ffa726","#795548","#455a64");
		$this->load->model("assignment_model");
		$fedderList = $this->assignment_model->getFedderList();
		$result = array();
		foreach($fedderList as $key => $fedderRow)
		{
			//$result[str_replace(" ","_",$fedderRow["s_subsection"])] = $colorArray[$key];
			$result[$fedderRow["s_subsection"]] = $colorArray[$key % sizeof($colorArray)];
		}
		echo json_encode($result,true);
	}

	public function getLineData()
	{
		$ogc_fid = $_POST["ogc_fid"];
		$table_name = $_POST["table"];
		$this->load->model("assignment_model");
		$lineData = $this->assignment_model->getLineData($ogc_fid,$table_name);
		echo json_encode($lineData);
	}

	public function getMotorApiList()
	{
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getMotorApiList();
		echo json_encode($result);
	}
	
	public function getMotorApiDetail($apiID)
	{
		
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getMotorApiData($apiID);
		echo json_encode($result);
		
	}
}