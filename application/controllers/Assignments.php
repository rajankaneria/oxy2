<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assignments extends CI_Controller {

    public function __construct(){
    	parent::__construct();
        if(!$this->session->userdata('fullname')){
			header("Location: ".base_url());
			exit();
		}
    }
	public function index()
	{
			$this->load->model("assignment_model");
			$this->load->model("user_model");
			$crewList = $this->assignment_model->getCrewList();
			$userRow = $this->user_model->currentUser();
			$this->assignment_model->chatCountCheck($this->session->userdata("userid"));

			$headerData = array(
				"pageTitle" => "Manage Assignments",
				"breadcrumb" => array(base_url()."dashboard"=>"Dashboard",base_url()."assignments"=>"Assignments")
			);
			$footerData = array(
				"jsFiles" => array("jquery.mCustomScrollbar.js","jquery.slimscroll.js","manage_assignments.js","chat.js")
			);
			$viewData = array(
				"viewName" => "manage_assignments",
				"viewData" => array("crewList"=>$crewList,"userRow"=>$userRow),
				"headerData" => $headerData,
				"footerData" => $footerData
			);
			$this->load->view('template',$viewData);
	}
	public function details($assignmentID)
	{
				$headerData = array(
					"pageTitle" => "Assignment Detail",
					"breadcrumb" => array(base_url()."dashboard"=>"Dashboard",base_url()."assignments/"=>"Assignments",base_url()."assignments/details/$assignmentID"=>"Details")
				);
				$footerData = array(
					"jsFiles" => array("jquery.geo-1.0.0-rc1.1.min.js","common.js","assignment_details.js","jquery-qrcode-0.14.0.min.js")
				);
				$viewData = array(
					"viewName" => "assignment_detail",
					"viewData" => array("assignmentID"=>$assignmentID),
					"headerData" => $headerData,
					"footerData" => $footerData
				);
				$this->load->view('template',$viewData);
  	}
  
	public function create()
  	{
		$this->load->model("assignment_model");

		$crewList = $this->assignment_model->getCrewList();
		$substationList = $this->assignment_model->getSubstationList();

		$headerData = array(
			"pageTitle" => "Create Assignment",
			"breadcrumb" => array(base_url()."dashboard"=>"Dashboard",base_url()."assignments/"=>"Assignments",base_url()."assignments/create"=>"New Assignment")
		);
		$footerData = array(
			"jsFiles" => array("jquery.slimscroll.js","jquery.geo-1.0.0-rc1.1.min.js","common.js","create_assignment.js","jquery-qrcode-0.14.0.min.js")
		);
    	$viewData = array(
			"viewName" => "create_assignment",
      		"viewData" => array("crewList"=>$crewList,"substationList"=>$substationList),
			"headerData" => $headerData,
			"footerData" => $footerData
		);
		$this->load->view('template',$viewData);
  	}
  	public function report()
  	{
		$this->load->model("assignment_model");

		$headerData = array(
			"pageTitle" => "Progress Report",
			"breadcrumb" => array(base_url()."dashboard"=>"Dashboard",base_url()."assignments/"=>"Assignments",base_url()."assignments/report"=>"Progress Report")
		);
		$footerData = array(
			"jsFiles" => array("jquery.slimscroll.js","jquery.geo-1.0.0-rc1.1.min.js","common.js","progress_report.js")
		);
    	$viewData = array(
			"viewName" => "progress_report",
      		"viewData" => array(),
			"headerData" => $headerData,
			"footerData" => $footerData
		);
		$this->load->view('template',$viewData);
  	}

	public function geomCheck($assignmentID)
	{
			$this->load->model("assignment_model");
			$result = $this->assignment_model->getGeometry($assignmentID);
			echo json_encode($result);
	}

	public function getAssignmentRow($assignmentID)
	{
			$this->load->model("assignment_model");
			$result = $this->assignment_model->getRow($assignmentID);
			echo json_encode($result);

	}

	public function getSubstationGeom($substationID)
	{
			$this->load->model("assignment_model");
			$substationGeom = $this->assignment_model->getSubstationGeom($substationID);
			echo json_encode($substationGeom,true);		
	}

	public function getFedderColors()
	{
		$colorArray = array("#ef5350","#ab47bc","#7e57c2","#5c6bc0","#42a5f5","#26c6da","#26c6da","#d4e157","#66bb6a","#ffa726","#795548","#455a64");
		$this->load->model("assignment_model");
		$fedderList = $this->assignment_model->getFedderList();
		$result = array();
		foreach($fedderList as $key => $fedderRow)
		{
			//$result[str_replace(" ","_",$fedderRow["s_subsection"])] = $colorArray[$key];
			$result[$fedderRow["s_subsection"]] = $colorArray[$key % sizeof($colorArray)];
		}
		echo json_encode($result,true);
	}

	public function getMotorData($ogc_fid)
	{
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getMotorRow($ogc_fid);
		echo json_encode($result);
	}

	public function getPoleData($ogc_fid)
	{
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getPoleRow($ogc_fid);
		echo json_encode($result);
	}

	public function getSubstationData($substationID)
	{
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getSubstationRow($substationID);
		echo json_encode($result);
	}

	public function saveAssignment()
	{
		$this->load->model("assignment_model");
		$assignmentData = $this->input->post("assignmentData");
		$result = $this->assignment_model->saveAssignment($assignmentData);
		var_dump($result);
	}

	public function deleteAssignment($assignmentID)
	{
		$this->load->model("assignment_model");
		$this->assignment_model->deleteAssignment($assignmentID);
	}

	public function reAssignCrew($assignmentID)
	{
		$this->load->model("assignment_model");
		$crewID = $this->input->post("crewID");
		$this->assignment_model->updateCrew($crewID,$assignmentID);
	}

	public function getAssignmentTypes()
	{	
		$this->load->model("assignment_model");
		$result = $this->assignment_model->assignmentTypes();
		echo json_encode($result);
		
	}

	public function getMotorApiList()
	{
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getMotorApiList();
		echo json_encode($result);
	}

	public function getMotorApiDetail($apiID)
	{
		
		$this->load->model("assignment_model");
		$result = $this->assignment_model->getMotorApiData($apiID);
		echo json_encode($result);
		
	}
	

	public function fixtable()
	{
		//$this->db->query("CREATE SEQUENCE s_assignments_id_seq START WITH 2 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;");
		//$this->db->query("CREATE SEQUENCE s_assignmentgeom_ogc_fid_seq START WITH 2 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;");
		//$query = $this->db->query("SELECT c.relname FROM pg_class c WHERE c.relkind = 'S'");
		//$result = $query->result_array();
		//var_dump($result);
		//$query = $this->db->query("ALTER TABLE s_assignments ALTER COLUMN id SET DEFAULT nextval('s_assignments_id_seq')");
		//$query = $this->db->query("ALTER TABLE s_assignmentgeom ALTER COLUMN ogc_fid SET DEFAULT nextval('s_assignmentgeom_ogc_fid_seq')");

		//$query = $this->db->query("ALTER TABLE s_assignments ADD COLUMN resolution character varying(255)");

		//$query = $this->db->query("CREATE TABLE s_assignmenttypes( id serial primary key, name VARCHAR(40) not null)");
		//$query = $this->db->query("CREATE TABLE s_userassignmentchatcount( id serial primary key, userid varchar(10), assignmentid VARCHAR(10), chat_count VARCHAR(10))");
		//$query = $this->db->query("CREATE TABLE s_pictures( id serial primary key, api_id varchar(20), picture VARCHAR(30), reviewed VARCHAR(10))");
		$this->db->query("update s_users set s_job_function_id=0 where id=4");
		$this->db->query("update s_users set s_job_function_id=6 where id=3");
	}

	public function assignmentJson()
	{
		$this->load->model("assignment_model");
		$assignmentList = $this->assignment_model->getAll();
		foreach($assignmentList as  $assignmentIndex => $assignmentRow)
		{
			$assignmentList[$assignmentIndex]["duration"] = floor((strtotime($assignmentRow["expecteddate"]) - strtotime($assignmentRow["assigndate"]))/ (60 * 60 * 24));
			$newChatCount = $this->assignment_model->getNewChatCount($this->session->userdata("userid"),$assignmentRow["id"]);
			$assignmentList[$assignmentIndex]["newChatCount"] = $newChatCount;
		}
		echo json_encode($assignmentList);
	}

	public function getLineData()
	{
		$ogc_fid = $_POST["ogc_fid"];
		$table_name = $_POST["table"];
		$this->load->model("assignment_model");
		$lineData = $this->assignment_model->getLineData($ogc_fid,$table_name);
		echo json_encode($lineData);
	}


}