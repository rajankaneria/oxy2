<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	public function index()
	{
		$headerData = array(
			"pageTitle" => "Signup"
		);
		$footerData = array(
			"jsFiles" => array("signup.js")
		);
		$viewData = array(
			"viewName" => "signup",
            "viewData" => "",
			"headerData" => $headerData,
			"footerData" => $footerData
		);
		$this->load->view('template',$viewData);
	}

	public function register()
	{
		$this->load->model("user_model");
		$userData = $this->input->post("userData");
		$userid = $this->user_model->register($userData);
		if($userid == 0)
		{
			echo "fail";
		}
		else
		{
			/*
			$email = $userData["email"];
			$full_name = $userData["first_name"]." ".$userData["last_name"];
			$data = array(
				"userid" => $userid,
				"email" => $email,
				"fullname" =>$full_name
			);
			$this->session->set_userdata($data);
			*/

			//code to send email should go over here
			echo "success";
		}

	}
	
}
