<?php 

class Chat extends CI_Controller {

    public function __construct(){
    	parent::__construct();
        if(!$this->session->userdata('fullname')){
			header("Location: ".base_url());
			exit();
		}
    }
    
    public function sendMsg()
    {
        $this->load->model("chat_model");

        $msg = $this->input->post("msg");
        $assignmentID = $this->input->post("assignmentID");

        $this->chat_model->saveChat($msg,$assignmentID);

        $chatArray = array(
            array(
                "userid" => $this->session->userdata("userid"),
                "userfullname" => $this->session->userdata("fullname"),
                "chattext" => $msg,
                "crdate" => date("Y-m-d H:i:s")
            )
        );
        $chatHtml = $this->load->view("chat_row",array("chatData" => $chatArray),TRUE);
        $output = array(
            "status" => "success",
            "chatHtml" => $chatHtml
        );
        echo json_encode($output);
    }

    public function getChat($assignmentID)
    {
        $this->load->model("chat_model");
        $chatArray = $this->chat_model->getChat($assignmentID);
        $chatHtml = $this->load->view("chat_row",array("chatData" => $chatArray),TRUE);
        $output = array(
            "status" => "success",
            "chatHtml" => $chatHtml
        );
        echo json_encode($output);
    }

}