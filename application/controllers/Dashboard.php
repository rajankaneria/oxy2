<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
    	parent::__construct();
        if(!$this->session->userdata('fullname')){
			header("Location: ".base_url());
			exit();
		}
    }
	
	public function index()
	{
		$this->load->model("assignment_model");
		
		$headerData = array(
			"pageTitle" => "Dashboard",
			"breadcrumb" => array("dashboard"=>"Dashboard")
		);
		$footerData = array(
			"jsFiles" => array()
		);
		$viewData = array(
			"viewName" => "dashboard",
            "viewData" => array(
				"assignment_completed" => $this->assignment_model->getCompletedCount(),
				"assignment_pending" => $this->assignment_model->getPendingCount(),
				"active_crew" => $this->assignment_model->getActiveCrewCount(),
				"pole_count" => $this->assignment_model->getPoleCount(),
				"motor_count" => $this->assignment_model->getMotorCount()
			),
			"headerData" => $headerData,
			"footerData" => $footerData	
		);
		$this->load->view('template',$viewData);
	}
}
