<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		
		if($this->session->userdata('fullname')){
			header("Location: ".base_url()."dashboard");
			exit();
		}
		$headerData = array(
			"pageTitle" => "Login"
		);
		$footerData = array(
			"jsFiles" => array("login.js")
		);
		$viewData = array(
			"viewName" => "login",
            "viewData" => "",
			"headerData" => $headerData,
			"footerData" => $footerData
		);
		$this->load->view('template',$viewData);
	}
	public function loginCheck()
	{
		$this->load->model("login_model");
		$email = $this->input->post("email");
		$password = $this->input->post("password");

		$query = $this->login_model->validate($email,$password);
		if($query)
		{
			$userid = $query["id"];
			$email = $query["email"];
			$full_name = $query["first_name"]." ".$query["last_name"];
			$data = array(
				"userid" => $userid,
				"email" => $email,
				"fullname" =>$full_name
			);
			$this->session->set_userdata($data);
			echo "success";
			
		}
		else	
		{
			echo "fail";		
		}
		
	}
	public function signout()
	{
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('fullname');
		$this->session->sess_destroy();
		header("Location: ../");
	}
	public function phpinformation()
	{
		phpinfo();
	}
}
