<?php 

class Chat_model extends CI_Model{

    function saveChat($msg,$assignmentID)
    {
        $this->load->model("assignment_model");
        $assignmentRow = $this->assignment_model->getRow($assignmentID);
        $chatData = array(
            "assignmentid" => $assignmentID,
            "userid" => $this->session->userdata("userid"),
            "chattext" => $msg,
            "crdate" => date("Y-m-d H:i:s"),
            "crew" => $assignmentRow["crew"]
        );

        $this->db->insert("s_assignmentchats",$chatData);
        $this->assignment_model->updateChatCount($this->session->userdata("userid"),$assignmentID);
    }

    function getChat($assignmentID)
    {
        $this->load->model("assignment_model");
        $this->assignment_model->updateChatCount($this->session->userdata("userid"),$assignmentID);
        $chatQuery = $this->db->query("
            select c.*,concat(u.first_name::text,' ', u.last_name::text) AS userfullname from s_assignmentchats c 
            join s_users u on u.id = c.userid
            where c.assignmentid='$assignmentID' order by c.id ASC
            ");
        $result = $chatQuery->result_array();
        return $result;
    }

}