<?php 

class Picture_model extends CI_Model{

	public function getAllPictures($currentUser)
	{
        if($currentUser["s_job_function_id"]>=6)
        {
            $query = $this->db->query("
                select p.fid,p.pictype,p.featuretable,p.comments,p.reviewdatetime,p.name,p.rotation,p.reviewedby,u.first_name,u.last_name,u.email from s_photos p 
                left join s_users u on p.reviewedby = u.id
                order by p.fid asc");
        }
        else
        {
            $currentUserid = $currentUser["id"];
            $query = $this->db->query("select fid,pictype,featuretable,comments,reviewdatetime,name,rotation,reviewedby from s_photos where reviewedby='$currentUserid' order by fid asc");
        }
		
		$result = $query->result_array();
		return $result;
	}

	public function savePicture($apiNo,$pictureName)
    {
        $this->db->query("insert into s_pictures(api_id,picture,reviewed) values('$apiNo','$pictureName','false')");
    }
    public function updatePicture($id)
    {
    	$this->db->query("update s_pictures set reviewed='true' where id='$id'");
    }
    public function updateImageData($imageData,$fid)
    {
    	$this->db->where('fid', $fid);
		$this->db->update('s_photos', $imageData); 
    }
    public function getPictureDetails($fid)
    {
        $query = $this->db->query("select * from s_photos where fid='$fid'");
        $result = $query->row_array();
        return $result;
    }
}