<?php

class database_model extends CI_Model{
		
	function table_list()
	{
		$tables = $this->db->list_tables();
		return $tables;
	}
	function field_list($table_name)
	{
		$fields = $this->db->field_data($table_name);
		return $fields;
	}
	function table_data($table_name)
	{
		$query = $this->db->get($table_name);
		$result = $query->result();
		var_dump($result);
	}
	function all_features()
	{
		$query = $this->db->query("
            select * from r_features where type='feature'
        ");
        $output = $query->result_array();
        return $output;
	} 
	function feature_data($api_no,$table_name)
	{
		$output = array();
		$tableCheck = $this->db->query("
			SELECT column_name 
			FROM information_schema.columns 
			WHERE table_name='$table_name' and column_name='api_no'
		");
		if($tableCheck->num_rows() != 0)
        {
			$query = $this->db->query("
	            select *,ST_AsText(wkb_geometry) as geometryWKT from $table_name where api_no='$api_no'
	        ");
	        $output = $query->row_array();
	    }
        return $output;
	} 
	function motor_edges($api_no)
	{
		$query = $this->db->query("select * from s_motor_nodes where api_id='$api_no'");
		$result = $query->result_array();
		$output = array();
		foreach ($result as $key => $edge_row) {
			$output[] = array(
				"from" => $edge_row["from_id"],
				"to" => $edge_row["to_id"]
			);
		}
		return json_encode($output);
	}

	function associated_lines($api_alphanumber)
	{
		$lines = array(
			"ohprimary" => $this->get_ohprimary($api_alphanumber),
			"ohsecondary" => $this->get_ohsecondary($api_alphanumber),
			"ugsecondary" => $this->get_ugsecondary($api_alphanumber),
			"ugprimary" => $this->get_ugprimary($api_alphanumber)
		);
		$output = array();
		foreach ($lines as $key => $lineArray) {
			$output = array_merge($output,$lineArray);
		}
		return $lines;
	}
	function get_ohprimary($api_alphanumber)
	{
		$query = $this->db->query("select fromguid,fromlayer,toguid,tolayer from ohprimaryline where api_alphanumber='$api_alphanumber'");
		$result = $query->result_array();
		return $result;
	}
	function get_ohsecondary($api_alphanumber)
	{
		$query = $this->db->query("select fromguid,fromlayer,toguid,tolayer from ohsecondaryline where api_alphanumber='$api_alphanumber'");
		$result = $query->result_array();
		return $result;
	}
	function get_ugsecondary($api_alphanumber)
	{
		$query = $this->db->query("select fromguid,fromlayer,toguid,tolayer from ugsecondaryline where api_alphanumber='$api_alphanumber'");
		$result = $query->result_array();
		return $result;
	}
	function get_ugprimary($api_alphanumber)
	{
		$query = $this->db->query("select fromguid,fromlayer,toguid,tolayer from ugprimaryline where api_alphanumber='$api_alphanumber'");
		$result = $query->result_array();
		return $result;
	}
	function getFeature($featureName,$featureID)
	{
		$tableQuery = $this->db->query("select postgresname from r_features where name='$featureName'");
		$tableResult = $tableQuery->row_array();
		$tableName = $tableResult["postgresname"];

		$featureFieldQuery = $this->db->query("select * from s_feature_fields where table_name='$tableName'");

		$featureFields = $featureFieldQuery->result_array();

		$featureQuery = $this->db->query("select * from $tableName where objectguid='$featureID'");
		$featureData = $featureQuery->row_array();
		$output = array(
			"fields" => $featureFields,
			"data" => $featureData
		);
		return $output;
	}


	function getLine($tableName,$to,$from)
	{
		$featureFieldQuery = $this->db->query("select * from s_feature_fields where table_name='$tableName'");
		$featureFields = $featureFieldQuery->result_array();

		$lineQuery = $this->db->query("select * from $tableName where fromguid='$from' and toguid='$to'");
		$lineData = $lineQuery->row_array();

		$output = array(
			"fields" => $featureFields,
			"data" => $lineData
		);
		return $output;
	}



	function getTableName($featureName)
	{
		$featureName = strtolower($featureName);
		$tableQuery = $this->db->query("select postgresname from r_features where name='$featureName'");
		$tableResult = $tableQuery->row_array();
		$tableName = $tableResult["postgresname"];
		return $tableName;
	}

	function getSubstationList()
	{
		$substationQuery = $this->db->query("select * from s_substations order by idsubstations ASC");
		$output = $substationQuery->result_array();
		return $output;
	}

	function saveSubstationField($substationID,$column,$value)
	{
		$columnTypeArray = explode("_", $column);
		$output = array_slice($columnTypeArray, 0, sizeof($columnTypeArray)-1);
		$dateColumn = implode("_", $output)."_date";
		$this->db->query("update s_substations set $column='$value',$dateColumn=current_timestamp where idsubstations='$substationID'");
	}
}
