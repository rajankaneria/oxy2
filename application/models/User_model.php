<?php 

class User_model extends CI_Model{
		
	function currentUser()
	{
        $userid = $this->session->userdata("userid");
		$query = $this->db->query("select * from s_users where id='$userid'");
		$result = $query->row_array();
        return $result;
	}

	function register($userData)
	{
		$email = $userData["email"];
		$emailCheck = $this->db->query("select * from s_users where email='$email'");
		if($emailCheck->num_rows() > 0)
		{
			return 0;
		}
		else
		{
			$userData["s_job_function_id"] = 0;
			$this->db->insert('s_users', $userData);
			$userid = $this->db->insert_id();
			return $userid;
		}
	}
	public function getAllUsers()
	{
		$query = $this->db->query("select * from s_users order by id asc");
		$result = $query->result_array();
		return $result;
	}
	public function updateJobFunction($userID,$jobFunction)
	{
		$this->db->query("update s_users set s_job_function_id='$jobFunction' where id='$userID'");
	}
	public function userData($userid)
	{
		$query = $this->db->query("select * from s_users where id='$userid'");
		$result = $query->row_array();
		return $result;
	}
	
}
