<?php 

class Assignment_model extends CI_Model{
		
	function getAll()
	{
        $query = $this->db->query("
            select a.*,c.crewname,s.name from s_assignments a
            join s_substations s on a.substation = s.idsubstations
            join s_crews c on a.crew = c.id
            order by a.id desc
        ");
        $output = $query->result_array();
        return $output;
	}

	function getRow($assignmentID)
	{
		$query = $this->db->query("select a.*,c.crewname,s.name from s_assignments a
            join s_substations s on a.substation = s.idsubstations
            join s_crews c on a.crew = c.id 
            where a.id='$assignmentID'");
        $output = $query->row_array();
        return $output;
	}

    function getPendingCount()
    {
        $query = $this->db->query("select id from s_assignments where status!='Completed'");
        $output = $query->num_rows();
        return $output;
    }

    function getCompletedCount()
    {
        $query = $this->db->query("select id from s_assignments where status='Completed'");
        $output = $query->num_rows();
        return $output;
    }

    function getActiveCrewCount()
    {
        $query = $this->db->query(" select distinct crew as crewID,c.crewname from s_assignments a join s_crews c on c.id=a.crew ");
        $result = $query->num_rows();
        return $result;
    }

    function getPoleCount()
    {
        $query = $this->db->query("select ogc_fid from pole");
        $output = $query->num_rows();
        return $output;
    }

    function getMotorCount()
    {
        $query = $this->db->query("select ogc_fid from motor");
        $output = $query->num_rows();
        return $output;
    }
    function getCrewList()
    {
        $query = $this->db->query("select id,crewname from s_crews");
        $output = $query->result_array();
        return $output;
    }
    function getSubstationList()
    {
        $query = $this->db->query("select idsubstations,name,x,y from s_substations where x IS NOT NULL and y IS NOT NULL");
        $output = $query->result_array();
        return $output;
    }
    function getGeometry($assignmentID)
    {
        $query = $this->db->query("
            select *,ST_AsText(wkb_geometry) as wkt from s_assignmentgeom
            where assignid='$assignmentID'
        ");
        $output = $query->result_array();
        return $output;
    }

    function getSubstationGeom($substationID)
    {
        $substationQuery = $this->db->query("select * from s_substations where idsubstations='$substationID'");
        $substationRow = $substationQuery->row_array();
        $substationName = $substationRow["name"];

        $poleGeom = $this->getPoleGeom($substationName);
        $motorGeom = $this->getMotorGeom($substationName);
        $OHlineGeom = $this->getOHlines($substationName);

        $result = array(
            "poles" => $poleGeom,
            "motors" => $motorGeom,
            "ohlines" => $OHlineGeom,
            "substation" => $substationRow
        );
        return $result;

    }

    function getPoleGeom($substationName)
    {
        $geomQuery = $this->db->query("select ST_AsText(wkb_geometry) as poleWKT,ogc_fid from pole where s_substation='$substationName'");
        $result = $geomQuery->result_array();
        return $result;
    }

    function getMotorGeom($substationName)
    {
        $geomQuery = $this->db->query("select ST_AsText(wkb_geometry) as motorWKT,ogc_fid,api_no,api_alphanumber from motor where s_substation='$substationName'");
        $result = $geomQuery->result_array();
        return $result;
    }
	
    function getOHlines($substationName)
    {
        $OHprimaryQuery = $this->db->query("select ST_AsText(wkb_geometry) as primaryWKT,s_subsection,ogc_fid from ohprimaryline where s_substation='$substationName'");
        $primaryLines = $OHprimaryQuery->result_array();
        $OHsecondaryQuery = $this->db->query("select ST_AsText(wkb_geometry) as secondaryWKT,s_subsection,ogc_fid from ohsecondaryline where s_substation='$substationName'");
        $secondaryLines = $OHsecondaryQuery->result_array();
        $result = array(
            "primary" => $primaryLines,
            "secondary" => $secondaryLines
        );
        return $result;
    }

    function getFedderList()
    {
        $fedderPrimaryQuery = $this->db->query("select distinct s_subsection from ohprimaryline");
        $primaryResult = $fedderPrimaryQuery->result_array();
        $fedderSecondaryQuery = $this->db->query("select distinct s_subsection from ohsecondaryline");
        $secondaryResult = $fedderSecondaryQuery->result_array();
        return array_merge($primaryResult,$secondaryResult);
    }

    function getMotorRow($ogc_fid)
    {
        $query = $this->db->query("select * from motor where ogc_fid='$ogc_fid'");
        $result = $query->row_array();
        return $result;
    }

    function getPoleRow($ogc_fid)
    {
        $query = $this->db->query("select * from pole where ogc_fid='$ogc_fid'");
        $result = $query->row_array();
        return $result;
    }

    function getSubstationRow($substationID)
    {
        $query = $this->db->query("select * from s_substations where idsubstations='$substationID'");
        $result = $query->row_array();
        return $result;
    }

    function saveAssignment($assignmentData)
    {
        $assignmentDetail = array(
            "assignmentid" => $assignmentData["assignmentid"],
            "assigndate" => date("Y-m-d"),
            "expecteddate" => date_format(new DateTime($assignmentData["expecteddate"]), 'Y-m-d'),
            "description" => $assignmentData["description"],
            "crew" => $assignmentData["crew"],
            "status" => "Assigned",
            "substation" => $assignmentData["substation"],
            "priority" => $assignmentData["priority"],
            "type" => $assignmentData["type"],
            "resolution" => $assignmentData["resolution"]
        );
        $this->saveType($assignmentData["type"]);
        $detailQuery = $this->db->insert("s_assignments",$assignmentDetail);
        $assignmentID = $this->db->insert_id();
        $queryString = "insert into s_assignmentgeom(wkb_geometry,assignid,cent_x,cent_y) values(ST_Multi(ST_GeomFromText('SRID=4326;".$assignmentData["wkb_geometry"]."')),'".$assignmentID."','".$assignmentData["cent_x"]."','".$assignmentData["cent_y"]."')";
        $geomQuery = $this->db->query($queryString);
        
        return $queryString;
    }
    function saveType($assignmentType)
    {
        $query = $this->db->query("select * from s_assignmenttypes where name='$assignmentType'");
        if($query->num_rows() == 0)
        {
            $this->db->query("insert into s_assignmenttypes(name) values('$assignmentType')");
        }
    }
    function deleteAssignment($assignmentID)
    {
        $this->db->query("delete from s_assignmentgeom where assignid='$assignmentID'");
        $this->db->query("delete from s_assignments where id='$assignmentID'");
    }
    function updateCrew($crewID,$assignmentID)
    {
        $this->db->query("update s_assignments set crew='$crewID' where id='$assignmentID'");
    }

    function assignmentTypes()
    {
        $query = $this->db->query("select * from s_assignmenttypes");
        $result = $query->result_array();
        return $result;
    }
    function getMotorApiList()
    {
        $query = $this->db->query("select distinct api_no from motor");
        $result = $query->result_array();
        return $result;
    }
    function getMotorApiData($apiID)
    {
        $query = $this->db->query("select x,y,ogc_fid from motor where api_no='$apiID'");
        $result = $query->row_array();
        return $result;
    }
    function chatCountCheck($userID)
    {
        $assignmentQuery = $this->db->query("select id from s_assignments");
        $assignmentList = $assignmentQuery->result_array();
        foreach($assignmentList as $key=>$assignmentData)
        {
            $assignmentID = $assignmentData["id"];
            $queryCheck = $this->db->query("select * from s_userassignmentchatcount where userid='$userID' and assignmentid='$assignmentID'");
            if($queryCheck->num_rows() == 0)
            {
                $this->addChatCountRow($userID,$assignmentID);
            }
        }
    }
    function addChatCountRow($userID,$assignmentID)
    {
        $insertData = array(
            "userid" => $userID,
            "assignmentid" => $assignmentID,
            "chat_count" => 0
        );
        $this->db->insert("s_userassignmentchatcount",$insertData);
    }
    function getNewChatCount($userID,$assignmentID)
    {
        $oldCountQuery = $this->db->query("select id,chat_count from s_userassignmentchatcount where userid='$userID' and assignmentid='$assignmentID'");
        $oldCountResult = $oldCountQuery->row_array();
        $oldCount = $oldCountResult["chat_count"];

        $currentCountQuery = $this->db->query("select id from s_assignmentchats where assignmentid='$assignmentID'");
        $currentCount = $currentCountQuery->num_rows();

        $result = $currentCount - $oldCount;
        return $result;
    }
    function updateChatCount($userID,$assignmentID)
    {
        $currentCountQuery = $this->db->query("select id from s_assignmentchats where assignmentid='$assignmentID'");
        $currentCount = $currentCountQuery->num_rows();

        $this->db->query("update s_userassignmentchatcount set chat_count='$currentCount' where userid='$userID' and assignmentid='$assignmentID'");
    }

    function getLineData($ogc_fid,$table_name)
    {
        $query = $this->db->query("select * from $table_name where ogc_fid='$ogc_fid'");
        $result = $query->row_array();
        return $result;
    }

    function motorSubstation($api_no)
    {
        $query = $this->db->query("select s.*,m.* from motor m join s_substations s on s.name = m.s_substation where m.api_no='$api_no'");
        $result = $query->row_array();
        return $result["idsubstations"];
    }
    function motorLocation($api_no)
    {
        $query = $this->db->query("select x,y from motor where api_no='$api_no'");
        $result = $query->row_array();
        return $result;
    }
	
}