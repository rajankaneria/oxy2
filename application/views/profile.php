
  
  

    <!-- login form -->
    <div class="row">
      <div class="col s12 m4 login-form">
        <div class="card z-depth-2">
            <div class="row card-content">

                <form class="col s12" autocomplete="false">
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <input id="first_name" name="first_name" type="text" value="<?php echo $first_name; ?>"  readonly onfocus="this.removeAttribute('readonly');">
                            <label for="first_name">Firstname</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <input id="last_name" name="last_name" type="text" value="<?php echo $last_name; ?>"  readonly onfocus="this.removeAttribute('readonly');">
                            <label for="last_name">Lastname</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <input disabled id="email" name="email" type="email" value="<?php echo $email; ?>">
                        <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <input id="phone" name="phone" type="text" value="<?php echo $phone; ?>"  readonly onfocus="this.removeAttribute('readonly');">
                        <label for="phone">Phone</label>
                        </div>
                    </div>
                </form>
                
            </div>
            <div class="row" style="height: 4px; margin-bottom:0px;">
                <div class="progress" style="margin:0px; display:none;">
                    <div class="indeterminate"></div>
                </div>
            </div>
            <div class="card-action login-btn-container">
                <div class="error-msg z-depth-1">Please fill all the details to continue!</div>
              <a class="waves-effect waves-light btn login-btn" id="saveBtn">Save</a>
            </div>

        </div>
      </div>
    </div>


  <!-- Modal Structure -->
  <div id="loginMsg" class="modal" style="width:30%;">
    <div class="modal-content">
      <p>Please check your login details and try to login again.</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>


  
