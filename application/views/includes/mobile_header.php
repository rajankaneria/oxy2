
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>CityGate Collector</title>
    <!-- CSS-->
    <link href="<?php echo base_url(); ?>html/assets/css/materialize.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>html/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>html/assets/css/mobile.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>html/assets/css/vis-network.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="<?php echo base_url(); ?>html/assets/kendo-ui/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>html/assets/js/vis.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQAJFUDjI0CoveaCT5jFHTG_hweSL15v8&libraries=drawing" async defer></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  </head>
  <body>
