
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <title>CityGate Collector</title>
    <!-- CSS-->
    <link href="<?php echo base_url(); ?>html/assets/css/materialize.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>html/assets/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>html/assets/kendo-ui/styles/kendo.default.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>html/assets/css/vis-network.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="<?php echo base_url(); ?>html/assets/js/vis.js"></script>
    <script src="<?php echo base_url(); ?>html/assets/kendo-ui/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>html/assets/kendo-ui/js/kendo.all.min.js"></script>
    <script src="<?php echo base_url(); ?>html/assets/js/jszip.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQAJFUDjI0CoveaCT5jFHTG_hweSL15v8&libraries=drawing" async defer></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
    <nav class="blue darken-3">
        <div class="nav-wrapper">
        <a href="#" class="logo">CityGate Collector - <?php echo $pageTitle; ?></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <?php if($this->session->userdata('fullname')){ ?>
            <li><a class="dropdown-button" href="#!" data-activates="userDropdown"><span id="userFullName"><?php echo $this->session->userdata('fullname'); ?></span><i class="material-icons right">arrow_drop_down</i></a></li>
            <?php }else{ ?>
            <li><a href="<?php echo base_url(); ?>signup">SignUp</a></li>
            <li><a href="<?php echo base_url(); ?>">Login</a></li>
            <?php } ?>
        </ul>
        <ul id="userDropdown" class="dropdown-content">
            <li><a href="<?php echo base_url(); ?>user/">Profile</a></li>
            <li class="divider"></li>
            <li><a href="<?php echo base_url(); ?>login/signout">Logout</a></li>
        </ul>
        </div>
    </nav>
    <?php if($this->session->userdata('fullname')){ ?>
    <nav class="main-breadcrumb blue darken-1">
        <div class="nav-wrapper">
        <div class="col s12">
            <?php foreach($breadcrumb as $link=>$pageName){ ?>
            <a href="<?php echo $link; ?>" class="breadcrumb"><?php echo $pageName; ?></a>
            <?php } ?>
        </div>
        </div>
        <div id="measureDistanceDisplay">Distance: <span>0.0</span></div>
    </nav>
    <?php } ?>