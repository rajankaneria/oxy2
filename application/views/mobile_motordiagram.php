<div class="container">
	<div class="row motor-diagram-container">
		<a id="mapSwitchBtn" href="<?php echo base_url(); ?>mobile/mapview?api_no=<?php echo $api_no; ?>" class="waves-effect waves-light btn btn-full">Switch to Map View</a>
		<div id="motorDiagram" class="col s12"></div>
	</div>
</div>

<!-- Modal Structure -->
<div id="featureDetailModal" class="modal">
    <div id="detailContainer" class="modal-content">
    </div>
    <div id="objectDetailFooter"><a class="modal-close waves-effect waves-light btn btn-full">Hide Details</a></div>
</div>

<div class="map-overlay">  
    <div class="preloader-wrapper small active map-preloader">
        <div class="spinner-layer spinner-blue-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>


<input id="api_no" type="hidden" value="<?php echo $api_no; ?>" />