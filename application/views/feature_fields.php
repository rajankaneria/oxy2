<input type="text" id="table_name" placeholder="table_name" /><br/>
<input type="text" id="field_name" placeholder="field_name" /><br/>
<input type="text" id="field_label" placeholder="field_label" /><br/>
<select id="is_link">
	<option value="0" selected="selected">No</option>
	<option value="1">Yes</option>
</select><br/>
<input type="button" value="submit" id="btn" />

<script src="/oxy2/html/assets/kendo-ui/js/jquery.min.js"></script>
<script>
$(function(){
	$("#btn").on("click",function(){
		var table_name = $("#table_name").val();
		var field_name = $("#field_name").val();
		var field_label = $("#field_label").val();
		var is_link = $("#is_link option:selected").val();
		$("#btn").attr("disabled","disabled");
		$.post("http://localhost/oxy2/database/insert_feature_field",{table_name:table_name,field_name:field_name,field_label:field_label,is_link:is_link},function(data){
			$("#field_name").val("");
			$("#field_label").val("");
			$("#btn").removeAttr("disabled");
		});
	});
});
</script>