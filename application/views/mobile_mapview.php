
        <div id="mapviewHeader" class="row">
            <div id="searchbarContainer" class="col s6">
                <div class="search-bar">
                    <div class="row" style="height: 4px; margin-bottom:0px;">
                        <div class="progress" style="margin:0px; display:none;">
                            <div class="indeterminate"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field">
                            <input id="searchField" placeholder="Search API ID"  type="text" class="validate  z-depth-1">
                        </div>
                        <a class="waves-effect waves-light btn" id="searchBtn"><i class="material-icons">search</i></a>
                    </div>
                </div>
            </div>

            <div id="substationContainer" class="col s6">
                <div class="input-field">
                    <select id="substation" class="browser-default">
                        <option value="first" disabled selected>Choose your Substation</option>
                        <?php foreach($substationList as $substationRow){ ?>
                        <option data-lat="<?php echo $substationRow['y']; ?>" data-lng="<?php echo $substationRow['x']; ?>" value="<?php echo $substationRow['idsubstations']; ?>"><?php echo $substationRow["name"]; ?></option>
                        <?php } ?>
                    </select>
                    <div class="substation-select-label">Select your substation</div>
                </div>
            </div>

        </div>



<div id="map"></div>
<input id="selectedSubstation" type="hidden" value="<?php echo $selectedSubstation; ?>" />
<input id="motorX" type="hidden" value="<?php echo $motorX; ?>" />
<input id="motorY" type="hidden" value="<?php echo $motorY; ?>" />
<div class="map-overlay">  
    <div class="preloader-wrapper small active map-preloader">
        <div class="spinner-layer spinner-blue-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>

<!-- Modal Structure -->
<div id="featureDetailModal" class="modal">
    <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2">x</a>
    <div id="detailContainer" class="modal-content">
    </div>
    <div id="objectDetailFooter"><a class="modal-close waves-effect waves-light btn btn-full">Hide Details</a></div>
</div>