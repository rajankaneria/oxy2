        
        
<?php foreach($chatData as $chatRow){ ?>
        <?php if($chatRow["userid"] == $this->session->userdata("userid")){ ?>
        <div class="chat-row">
            <div class="chat-to">
                <div class="username"><?php echo $chatRow["userfullname"]; ?></div>
                <div class="msg blue darken-3"><?php echo $chatRow["chattext"]; ?></div>
                <div class="details"><?php echo date_format(new DateTime($chatRow["crdate"]), 'Y-m-d h:iA'); ?></div>
            </div>
        </div>
        <?php }else{ ?>
        <div class="chat-row">
            <div class="chat-from">
                <div class="username"><?php echo $chatRow["userfullname"]; ?></div>
                <div class="msg teal lighten-2"><?php echo $chatRow["chattext"]; ?></div>
                <div class="details"><?php echo date_format(new DateTime($chatRow["crdate"]), 'Y-m-d h:iA'); ?></div>
            </div>
        </div>
        <?php } ?>
<?php } ?>