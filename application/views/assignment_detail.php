

<div class="card-panel hoverable create-panel" id="assignmentDetails" style="display:none;"></div>
<div class="map-overlay">  
    <div class="preloader-wrapper small active map-preloader">
        <div class="spinner-layer spinner-blue-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>
<div id="map"></div>
<a class="waves-effect waves-dark btn distance-btn white black-text" id="measureDistance"><span>Measure Distance</span></a>
<div class="search-bar">
    <div class="row" style="height: 4px; margin-bottom:0px;">
        <div class="progress" style="margin:0px; display:none;">
            <div class="indeterminate"></div>
        </div>
    </div>
    <div class="row">
        <div class="input-field" style="float:left;">
            <input id="searchField" placeholder="Search API ID"  type="text" class="validate  z-depth-1">
        </div>
        <a class="waves-effect waves-light btn" id="searchBtn"><i class="material-icons">search</i></a>
    </div>
</div>



<div id="objectDetail" class="card-panel hoverable" style="display:none;">
    <div id="mainContent">
    </div>
    <div id="objectDetailFooter"><a class="waves-effect waves-light btn btn-full">Hide Details</a></div>
</div>
<input id="assignmentID" type="hidden" value="<?php echo $assignmentID; ?>" />

<!-- Modal Structure -->
<div id="qrModal" class="modal">
    <div class="modal-content">
        <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2 tooltipped" data-position="left" data-delay="0" data-tooltip="Close">x</a>
        <div class="row">
            <div id="qrCode" class="col s12 offset-m4 m4 l4"></div>
            <div id="qrCodeLink" class="col s12 offset-m4 m4 l4"></div>
        </div>
    </div>
</div>

<!-- Modal Structure -->
<div id="motorModal" class="modal">
    <div class="modal-content">
        <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2 tooltipped" data-position="left" data-delay="0" data-tooltip="Close">x</a>
        <div class="motor-map-overlay">  
            <div class="preloader-wrapper small active map-preloader">
                <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div id="motorDiagramContainer"></div><!--<center><canvas id="myCanvas" width="600" height="600"></canvas></center>-->
            <div id="featureDetail" class="card-panel hoverable">
                <div id="hideFeatureDetailBtn"><a class="waves-effect waves-light btn btn-full">Hide Details</a></div>
                <div id="detailContainer"></div>
            </div>
        </div>
    </div>
</div>