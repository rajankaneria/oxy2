    <div class="main-container-full">
    
        <div class="row">
            <div class="col s12 m10 left">
            
                <div id="grid">
                </div>
            
            </div>
            <div class="col s12 m2 right">
            
                <div class="card-panel hoverable blue darken-3">
                    <a class="waves-effect waves-light btn btn-full" id="updateJobFunction">Update Job Function</a>
                </div>
            
            </div>
            
        </div>
    
    </div>

  <!-- Modal Structure -->
  <div id="jobFunctionModal" class="modal" style="width:30%;">
    <div class="modal-content">
        <h4>Update Job Function</h4>
        <div id="selectContainer">
            <select id="jobFunction" class="browser-default">
                <?php for($i=1;$i<=7;$i++){ ?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                <?php } ?>
            </select>
        </div>

    </div>
    <div class="row" style="height: 4px; margin-bottom:0px;">
        <div class="progress" style="margin:0px; display:none;">
            <div class="indeterminate"></div>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action waves-effect waves-green btn" style="margin-left:10px;" id="saveJobfunctionBtn">Save</a> <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>


  <!-- Modal Structure -->
  <div id="userMsg" class="modal" style="width:30%;">
    <div class="modal-content">
      <p>Please select any one User to proceed.</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>