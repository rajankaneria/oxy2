    <div class="main-container-full">
    
        <div class="row">
            <div class="col s12 m10 left">
                <div class="row" id="gridTools" style="margin-bottom: 0px; display:none;">
                  <div class="input-field col s12 m2">
                    <a id="selectAllGridRow" class="waves-effect waves-light btn btn-full">Select all</a>
                  </div>
                  <div class="input-field col s12 m10">
                    <input type="text" id="gridSearch"/>
                    <label for="gridSearch">Search Substation by Name</label>
                  </div>
                </div>

                <div id="grid">
                </div>
            
            </div>
            <div class="col s12 m2 right">
            
                <div class="card-panel hoverable blue darken-3">
                    <!--<a class="waves-effect waves-light btn btn-full" id="addPicture">Add Picture</a> -->
                    <a class="waves-effect waves-light btn btn-full" id="loadFields">Update Progress</a>
                </div>
            
            </div>
            
        </div>
    
    </div>







<!-- Modal Structure -->
  <div id="fieldListModal" class="modal modal-fixed-footer" style="width:70%;">
    <div class="modal-content">
        <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2 tooltipped" data-position="left" data-delay="0" data-tooltip="Close">x</a>
        <h4>Update Report</h4>
        
        <div id="fieldListGrid"></div>

    </div>
    <div class="row" style="height: 4px; margin-bottom:0px;">
        <div class="progress" style="margin:0px; display:none;">
            <div class="indeterminate"></div>
        </div>
    </div>
    <!--
    <div class="modal-footer">
      <a href="#!" class=" modal-action waves-effect waves-green btn" style="margin-left:10px;" id="assignUserBtn">Save</a> <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  -->



    <div id="fieldGridOverlay">

         <div id="statusContainer" class="row field-input">
            <div class="field-input-card card-panel">
              <div class="row">
                <div class="input-field col s12">
                    <select id="fieldStatus" class="browser-default field-status-select">
                        <option value="Not Started">Not Started</option>
                        <option value="In Process">In Process</option>
                        <option value="Completed">Completed</option>
                        <option value="Update">Update</option>
                    </select> 
                    <select id="accptanceFieldStatus" class="browser-default field-status-select">
                        <option value="Not Started">Not Started</option>
                        <option value="In Process">In Process</option>
                        <option value="Accepted">Accepted</option>
                        <option value="Needs Edits">Needs Edits</option>
                        <option value="Rejected">Rejected</option>
                    </select>
                </div>
              </div>
              <div class="row" style="height: 4px; margin-bottom:0px;">
                <div class="progress" style="margin:0px; display:none;">
                    <div class="indeterminate"></div>
                </div>
            </div>
              <div class="row">
                  <div class="col s12 m6">
                      <a href="#!" class="waves-effect waves-green btn grey btn-full overlay-close-btn">Close</a>
                  </div>
                  <div class="col s12 m6">
                      <a href="#!" class="waves-effect waves-green btn btn-full field-save-btn" id="saveStatus">Save</a>
                  </div>
              </div>
            </div>
        </div>

         <div id="commentContainer" class="row field-input">
            <div class="field-input-card card-panel">
              <div class="row">
                  <div class="input-field col s12">
                      <textarea id="fieldComment" class="materialize-textarea"></textarea>
                      <label for="fieldComment">Add your comment</label>
                  </div>
              </div>
              <div class="row" style="height: 4px; margin-bottom:0px;">
                <div class="progress" style="margin:0px; display:none;">
                    <div class="indeterminate"></div>
                </div>
            </div>
              <div class="row">
                  <div class="col s12 m6">
                      <a href="#!" class="waves-effect waves-green btn grey btn-full overlay-close-btn">Close</a>
                  </div>
                  <div class="col s12 m6">
                      <a href="#!" class="waves-effect waves-green btn btn-full field-save-btn" id="saveComment">Save</a>
                  </div>
              </div>
            </div>
        </div> 

    </div>

    <div class="modal-footer">
      <a href="#!" class="waves-effect waves-green btn" id="sendNotification">Done</a>
    </div>


  </div>










  <!-- Modal Structure -->
  <div id="reportMsg" class="modal" style="width:30%;">
    <div class="modal-content">
      <p>Please select any one substation to proceed.</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>


  <!-- Modal Structure -->
  <div id="fieldCommentModal" class="modal" style="width:30%;">
    <div class="modal-content">
        <div class="row">
            <div class="input-field col s12">
                <textarea id="fieldComment" class="materialize-textarea"></textarea>
                <label for="fieldComment">Add your comment</label>
            </div>
        </div>
    </div>
    <!--
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
    -->
  </div>

    <!-- Modal Structure -->
  <div id="fieldStatusModal" class="modal" style="width:30%;">
    <div class="modal-content">
        <div class="row">
            <div class="input-field col s12">
                <select id="statusSelect" class="browser-default">
                    <option>Not Started</option>
                    <option>In Process</option>
                    <option>Completed</option>
                    <option>Update</option>
                </select>
            </div>
        </div>
    </div>
    <!--
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
    -->
  </div>















<link href="<?php echo base_url(); ?>html/assets/kendo-ui/styles/kendo.common.min.css" rel="stylesheet">
<style>
.k-header .k-link{
   text-align: center;
}
label.checkbox-label {
    color: #2e2e2e;
}

.k-state-selected .checkbox-label {
    color: #fff;
}
.k-state-selected{
      background-color: #f35800 !important;
}
#fieldCommentModal .modal-content{
    padding-top: 50px;
}
</style>