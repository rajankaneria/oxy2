<?php foreach($assignmentList as $assignmentRow){ ?>
<tr class="assignment-row" id="assignment<?php echo $assignmentRow['id']; ?>">
    <!--
    <td data-field="checkbox"><input data-assignmentid="<?php echo $assignmentRow['id']; ?>" data-crewid="<?php echo $assignmentRow['crew']; ?>" type="radio" class="with-gap" id="filled-in-box<?php echo $assignmentRow['id']; ?>" name="assignment" /><label for="filled-in-box<?php echo $assignmentRow['id']; ?>"></label></td>
    -->
    <td data-field="assignmentID" id="assignmentid"><?php echo $assignmentRow["id"]; ?></td>
    <td data-field="id" id="assignmentName"><?php echo $assignmentRow["assignmentid"]; ?></td>
    <td data-field="type"><?php echo $assignmentRow["type"]; ?></td>
    <td data-field="priority"><?php echo $assignmentRow["priority"]; ?></td>
    <td data-field="assignmentDate"><?php echo $assignmentRow["assigndate"]; ?></td>
    <td data-field="expectedDate"><?php echo $assignmentRow["expecteddate"]; ?></td>
    <td data-field="duration"><?php echo floor((strtotime($assignmentRow["expecteddate"]) - strtotime($assignmentRow["assigndate"]))/ (60 * 60 * 24)); ?></td>
    <td data-field="status"><?php echo $assignmentRow["status"]; ?></td>
    <td data-field="description"><?php echo $assignmentRow["description"]; ?></td>
    <td data-field="substation"><?php echo $assignmentRow["name"]; ?></td>
    <td data-field="crew" id="crewName"><?php echo $assignmentRow["crewname"]; ?></td>
</tr>
<?php } ?>