<!DOCTYPE HTML>
<html>
  <head>
        <script src="/oxy2/html/assets/kendo-ui/js/jquery.min.js"></script>

    <style>
      body {
        margin: 0px;
        padding: 0px;
      }
      #myCanvas{
        padding:10px;
      }
    </style>
  </head>
  <body>
    
    <script>
    $(function(){
        plotMotorDiagram(<?php echo $api_no; ?>);
    });

    
    </script>
    <input type="hidden" value='<?php echo json_encode($canvasPoints); ?>' id="canvasPoints" />
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" />
  </body>
</html>  