
  
  

    <!-- login form -->
    <div class="row">
      <div class="col s12 m4 login-form">
        <div class="card z-depth-2">
          
            <div class="row card-content">
                <h4 class="center-align">Login</h4>
                <form class="col s12" autocomplete="false">
                    <div class="row">
                        <div class="input-field col s12">
                        <input id="loginEmail" name="loginEmail" type="email" class="validate" value="">
                        <label for="loginEmail">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <input id="loginPassword" name="loginPassword" type="password" class="validate" value="">
                        <label for="loginPassword">Password</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row" style="height: 4px; margin-bottom:0px;">
                <div class="progress" style="margin:0px; display:none;">
                    <div class="indeterminate"></div>
                </div>
            </div>
            <div class="card-action login-btn-container">

              <a class="waves-effect waves-light btn login-btn" id="loginBtn">Login</a>
            </div>

        </div>
      </div>
    </div>


  <!-- Modal Structure -->
  <div id="loginMsg" class="modal" style="width:30%;">
    <div class="modal-content">
      <p>Please check your login details and try to login again.</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>


  
