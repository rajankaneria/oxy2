
            <div class="card-panel hoverable create-panel" id="createAssignmentForm">
                <div id="createFieldContainer">

                    <div class="row">
                        <div class="input-field">
                            <input id="assignment_id" placeholder="Assignment ID" type="text" class="validate">
                        </div>
                        <div class="input-field">
                            <input id="expected_date" placeholder="Expected Date"  type="date" class="datepicker">
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field">
                            <select id="crew">
                                <option value="first" disabled selected>Choose your Crew</option>
                                <?php foreach($crewList as $crewRow){ ?>
                                <option value="<?php echo $crewRow['id']; ?>"><?php echo $crewRow["crewname"]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field">
                            <select id="priority">
                                <option value="first" disabled selected>Choose Assignment Priority</option>
                                <option value="High">High</option>
                                <option value="Normal">Normal</option>
                                <option value="Low">Low</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field">
                            <input id="assignmentType" placeholder="Assignment Type"  type="text" class="validate">
                        </div>
                    </div>

                    <div class="row" style="margin-top: 15px;">
                        <div class="input-field">
                            <select id="substation" class="browser-default">
                                <option value="first" disabled selected>Choose your Substation</option>
                                <?php foreach($substationList as $substationRow){ ?>
                                <option data-lat="<?php echo $substationRow['y']; ?>" data-lng="<?php echo $substationRow['x']; ?>" value="<?php echo $substationRow['idsubstations']; ?>"><?php echo $substationRow["name"]; ?></option>
                                <?php } ?>
                            </select>
                            
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field">
                            <textarea  id="description" placeholder="Description" class="materialize-textarea"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field">
                            <textarea  id="resolution" placeholder="Resolution" class="materialize-textarea"></textarea>
                        </div>
                    </div>
                </div>
                
                <input type="hidden" value="" id="polygonwkt" />
                <input type="hidden" value="" id="cent_x" />
                <input type="hidden" value="" id="cent_y" />
                <a class="btn-half btn grey lighten-3 grey-text text-darken-2" id="drawPolygonBtn">Draw</a>
                <a class="btn-half btn grey lighten-3 grey-text text-darken-2" disabled id="deactivatePolygonBtn">Deactivate </a>
                <a class="btn-full btn grey lighten-3 grey-text text-darken-2" disabled id="clearPolygonBtn">Clear Assignment Area</a>
                <div class="row" style="height: 4px; margin-bottom:0px;">
                    <div class="progress" style="margin:0px; display:none;">
                        <div class="indeterminate"></div>
                    </div>
                </div>
                <a class="waves-effect waves-light btn btn-full" id="saveBtn">Save</a>

                <div class="error-msg z-depth-1 center-align">Please fill all the details to continue!</div>


            </div>

<div class="map-overlay">  
    <div class="preloader-wrapper small active map-preloader">
        <div class="spinner-layer spinner-blue-only">
        <div class="circle-clipper left">
            <div class="circle"></div>
        </div><div class="gap-patch">
            <div class="circle"></div>
        </div><div class="circle-clipper right">
            <div class="circle"></div>
        </div>
        </div>
    </div>
</div>
<div id="map"></div>
<a class="waves-effect waves-dark btn distance-btn white black-text" id="measureDistance"><span>Measure Distance</span></a>

<div class="search-bar">
    <div class="row" style="height: 4px; margin-bottom:0px;">
        <div class="progress" style="margin:0px; display:none;">
            <div class="indeterminate"></div>
        </div>
    </div>
    <div class="row">
        <div class="input-field" style="float:left;">
            <input id="searchField" placeholder="Search API ID"  type="text" class="z-depth-1">
        </div>
        <a class="waves-effect waves-light btn" id="searchBtn"><i class="material-icons">search</i></a>
    </div>
</div>

<div id="objectDetail" class="card-panel hoverable" style="display:none;">
    <div id="mainContent">
    </div>
    <div id="objectDetailFooter"><a class="waves-effect waves-light btn btn-full">Hide Details</a></div>
</div>
<!-- <div id="legend"><h4>Legend</h4></div> -->

<!-- Modal Structure -->
<div id="qrModal" class="modal">
    <div class="modal-content">
        <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2 tooltipped" data-position="left" data-delay="0" data-tooltip="Close">x</a>
        <div class="row">
            <div id="qrCode" class="col s12 offset-m4 m4 l4"></div>
            <div id="qrCodeLink" class="col s12 offset-m4 m4 l4"></div>
        </div>
    </div>
</div>

<!-- Modal Structure -->
<div id="motorModal" class="modal">
    <div class="modal-content">
        <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2 tooltipped" data-position="left" data-delay="0" data-tooltip="Close">x</a>
        <div class="motor-map-overlay">  
            <div class="preloader-wrapper small active map-preloader">
                <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                    <div class="circle"></div>
                </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="motorDiagramContainer"></div><!--<center><canvas id="myCanvas" width="600" height="600"></canvas></center>-->
            <div id="featureDetail" class="card-panel hoverable">
                <div id="hideFeatureDetailBtn"><a class="waves-effect waves-light btn btn-full">Hide Details</a></div>
                <div id="detailContainer"></div>
            </div>
        </div>

    </div>
</div>
