


<div class="main-container">
    <div class="collection">
        <a href="#!" class="collection-item">Assignments Completed<span class="badge"><?php echo $assignment_completed; ?></span></a>
        <a href="#!" class="collection-item">Assignments Pending<span class="badge"><?php echo $assignment_pending; ?></span></a>
        <a href="#!" class="collection-item">Crews Active<span class="badge"><?php echo $active_crew; ?></span></a>
        <a href="#!" class="collection-item">Number of Wells collected<span class="badge"><?php echo $motor_count; ?></span></a>
        <a href="#!" class="collection-item">Number of Poles collected<span class="badge"><?php echo $pole_count; ?></span></a>
    </div>
    <div class="row" style="margin-top:100px;">
        <div class="card-panel hoverable col s12 m5 left blue darken-3" style="height:100px; cursor:pointer;" onclick="window.location.href='assignments'">
            <div class="valign-wrapper" style="height:100%;">
                <h5 class="valign center-align white-text" style="width:100%;">Manage Assignments</h5>
            </div>           
        </div>
        <div class="card-panel hoverable col s12 m5 right blue darken-3" style="height:100px; cursor:pointer;" onclick="window.location.href='database'">
            <div class="valign-wrapper" style="height:100%;">
                <h5 class="valign center-align white-text" style="width:100%;">View Database</h5>
            </div> 
        </div>
    </div>
</div>