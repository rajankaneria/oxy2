    <div class="main-container-full">
    
        <div class="row">
            <div class="col s12 m10 left">
                <input type="button" id="selectAllGridRow" value="select all" style="padding: 5px 35px;" />
                <div id="grid">
                </div>
            
            </div>
            <div class="col s12 m2 right">
            
                <div class="card-panel hoverable blue darken-3">
                    <!--<a class="waves-effect waves-light btn btn-full" id="addPicture">Add Picture</a> -->
                    <a class="waves-effect waves-light btn btn-full" id="reviewPicture">Review Picture</a>
                    <?php if($currentUser["s_job_function_id"] >= 6){ ?><a class="waves-effect waves-light btn btn-full" href="#" id="assignUser">Assign User</a><?php } ?>
                </div>
            
            </div>
            
        </div>
    
    </div>



<!-- Modal Structure -->
<div id="reviewModal" class="modal">
    <div class="modal-content">
        <h4>Review Picture</h4>
        <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2 tooltipped" data-position="left" data-delay="0" data-tooltip="Close">x</a>
        
        <div class="row picture-control">
            <center>
            <a class="waves-effect waves-light btn" href="#" id="previousImage">Previous</a>
            <a class="waves-effect waves-light btn" href="#" id="rotateLeft">Rotate Left</a>
            <a class="waves-effect waves-light btn" href="#" id="rotateRight">Rotate Right</a>
            <a class="waves-effect waves-light btn" href="#" id="nextImage">Next</a>
            <input type="hidden" value="" id="currentImageIndex" />
        </center>
        </div>
        <div id="pictureContainer">

        </div>
    </div>
</div>


<!-- Modal Structure -->
  <div id="assignUserModal" class="modal" style="width:30%;">
    <div class="modal-content">
        <h4>Assign User to Picture</h4>
        <div id="selectContainer">
            <select id="userList" class="browser-default">
                <?php foreach ($userList as $key => $userRow) { ?>
                <option value="<?php echo $userRow['id']; ?>"><?php echo $userRow['first_name']." ".$userRow["last_name"]." (".$userRow["email"].")"; ?></option>
                <?php } ?>
            </select>
        </div>

    </div>
    <div class="row" style="height: 4px; margin-bottom:0px;">
        <div class="progress" style="margin:0px; display:none;">
            <div class="indeterminate"></div>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action waves-effect waves-green btn" style="margin-left:10px;" id="assignUserBtn">Save</a> <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>

<!-- Modal Structure -->
<!--
<div id="addPictureModal" class="modal">
    <div class="modal-content">
        <h4>Add Picture</h4>

        <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2 tooltipped" data-position="left" data-delay="0" data-tooltip="Close">x</a>
        <div class="row">
            <div class="col s12 m2 left">
                <div class="input-field">
                    <input type="text" value="" id="api_no" />
                    <label for="api_no">API Number</label>
                </div>
            </div>
            <div class="col s12 m8 left">
                <div class="input-field">
                    <input type="text" value="" id="imageUrl" />
                    <label for="imageUrl">Image URL</label>
                </div>
            </div>
            <div class="col s12 m2 right">
                <a class="waves-effect waves-light btn right" href="#" id="getImage">Get Image</a>
                
            </div>
             
        </div>
        <div class="row">
            <a class="waves-effect waves-light btn hide right" href="#" id="saveImage">Save</a> <span class="error-msg">Please Enter API number to save the Image</span>
        </div>
        <div id="imageContainer">

        </div>
    </div>
</div>
-->

  <!-- Modal Structure -->
  <div id="pictureMsg" class="modal" style="width:30%;">
    <div class="modal-content">
      <p>Please select any one picture to proceed.</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>

<link href="<?php echo base_url(); ?>html/assets/kendo-ui/styles/kendo.common.min.css" rel="stylesheet">
<style>
.k-header .k-link{
   text-align: center;
}
label.checkbox-label {
    color: #2e2e2e;
}

.k-state-selected .checkbox-label {
    color: #fff;
}
</style>
