
  
  

    <!-- login form -->
    <div class="row">
      <div class="col s12 m4 login-form">
        <div class="card z-depth-2">
          
            <div class="row card-content">

                <form class="col s12" autocomplete="false">
                    <div class="row">
                        <div class="input-field col s12 m6">
                            <input id="first_name" name="first_name" type="text" value="" class="validate"  readonly onfocus="this.removeAttribute('readonly');">
                            <label for="first_name">Firstname</label>
                        </div>
                        <div class="input-field col s12 m6">
                            <input id="last_name" name="last_name" type="text" value="" class="validate"  readonly onfocus="this.removeAttribute('readonly');">
                            <label for="last_name">Lastname</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <input id="email" name="email" type="email" value="" class="validate"  readonly onfocus="this.removeAttribute('readonly');">
                        <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <input id="password" name="password" type="password" value="" class="validate"  readonly onfocus="this.removeAttribute('readonly');">
                        <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                        <input id="phone" name="phone" type="text" value=""  class="validate" readonly onfocus="this.removeAttribute('readonly');">
                        <label for="phone">Phone</label>
                        </div>
                    </div>
                </form>

            </div>
            <div class="row" style="height: 4px; margin-bottom:0px;">
                <div class="progress" style="margin:0px; display:none;">
                    <div class="indeterminate"></div>
                </div>
            </div>
            <div class="card-action login-btn-container">
                <div class="error-msg z-depth-1">Please fill all the details to continue!</div>
              <a class="waves-effect waves-light btn login-btn" id="signupBtn">Signup</a>
            </div>

        </div>
      </div>
    </div>


  <!-- Modal Structure -->
  <div id="signupErrorMsg" class="modal" style="width:30%;">
    <div class="modal-content">
        <h4>Error</h4>
        <p>Email you entered is already registered on our database. Please try to register with another email.</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>

  <div id="signupSuccessMsg" class="modal" style="width:30%;">
    <div class="modal-content">
        <h4>Success</h4>
        <p>Signed up successfully. Our admin will review your details and will grant access to our website. Thanks! </p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>


  
