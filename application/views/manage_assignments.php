    
    
    
    
    <div class="main-container-full">
    
        <div class="row">
            <div class="col s12 m10 left">
            
                <div id="grid">
                </div>
            
            </div>
            <div class="col s12 m2 right">
            
                <div class="card-panel hoverable blue darken-3">
                    <a class="waves-effect waves-light btn btn-full" href="#" id="openAssignment">View Assignment</a>
                    <a class="waves-effect waves-light btn btn-full" id="openChat">Open Chat</a>
                    <a class="waves-effect waves-light btn btn-full" id="deleteAssignment">Delete Assignment</a>
                    <a class="waves-effect waves-light btn btn-full" href="<?php echo base_url(); ?>assignments/create">Create Assignment</a>
                    <a class="waves-effect waves-light btn btn-full" id="reassignCrewBtn">Re-assign</a>
                    <?php if($userRow["s_job_function_id"] >= 5){ ?><a class="waves-effect waves-light btn btn-full" href="<?php echo base_url(); ?>pictures/review">Review Pictures</a><?php } ?>
                    <?php if($userRow["s_job_function_id"] >= 6){ ?><a class="waves-effect waves-light btn btn-full" href="<?php echo base_url(); ?>user/jobfunction">Job Function</a><?php } ?>
                    <a class="waves-effect waves-light btn btn-full" href="<?php echo base_url(); ?>assignments/report" id="reassignCrewBtn">Progress Report</a>
                </div>
            
            </div>
            
        </div>
    
    
    </div>


  <!-- Modal Structure -->
  <div id="chatModal" class="modal">
    <div class="modal-content">
      <h4></h4>
     <a class="waves-effect modal-close waves-light btn chat-close-btn red darken-2 tooltipped" data-position="left" data-delay="0" data-tooltip="Close Chat">x</a>
     <div id="chatContainer">

     </div>
        <div class="row">
            <div class="input-field" style="width: calc(100% - 130px); display:inline-block;">
                <input id="chat_msg" type="text">
                <label for="chat_msg">Write Message</label>
            </div>
            <div class="input-field" style="display:inline-block; float:right;">
                <a class="waves-effect waves-light btn" id="submitChat">Submit</a>
            </div>
        </div>
    </div>
</div>
    


  <!-- Modal Structure -->
  <div id="assignmentMsg" class="modal" style="width:30%;">
    <div class="modal-content">
      <p>Please select any one assignment to proceed.</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>

  <!-- Modal Structure -->
  <div id="deleteConfirmMsg" class="modal" style="width:30%;">
    <div class="modal-content">
      <p>Are you sure? Please press confirm to delete selected Assignment.</p>
    </div>
    <div class="row" style="height: 4px; margin-bottom:0px;">
        <div class="progress" style="margin:0px; display:none;">
            <div class="indeterminate"></div>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action waves-effect waves-green btn red lighten-2" style="margin-left:10px;" id="confirmDeleteAssignmentBtn">Confirm</a> <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>

  <!-- Modal Structure -->
  <div id="reAssignModal" class="modal" style="width:30%;">
    <div class="modal-content">
        <h4>Re-assign Crew</h4>
        <div id="selectContainer">
            <select id="crewReassign" class="browser-default">
                <?php foreach($crewList as $crewRow){ ?>
                <option value="<?php echo $crewRow['id']; ?>"><?php echo $crewRow["crewname"]; ?></option>
                <?php } ?>
            </select>
        </div>

    </div>
    <div class="row" style="height: 4px; margin-bottom:0px;">
        <div class="progress" style="margin:0px; display:none;">
            <div class="indeterminate"></div>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action waves-effect waves-green btn" style="margin-left:10px;" id="saveCrewBtn">Save</a> <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Close</a>
    </div>
  </div>
  



  <style>
  
  .msg {
    background: #cccccc;
    /* float: left; */
    display: inline-block;
    padding: 6px;
    font-size: 14px;
    -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;
    color:#fff;
    min-width: 100px;
}

.chat-to {
    float: right;
}
.chat-to .msg{
    text-align:right;
}

.username {
    text-align: right;
    color:#000;
    font-size:11px;
}

.details {
    text-align: right;
   color:#fff;
   font-size:11px;
   margin-top:2px;
}
.chat-row{
    overflow:hidden;
}
.chat-row:hover .username{
    color:#000;
}
.chat-row:hover .details{
    color:#000;
}

.chat-from{
    float:left;
}
.chat-from .username{
    text-align:left;
}
.chat-from .details{
    text-align:left;
}
.chat-from .msg{
    text-align:left;
}
#chatContainer{
    height:400px;
}
  
  </style>
  <link href="/oxy2/html/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
  
    