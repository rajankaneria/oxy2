 /* ------------- Constant Declaration------------- */
    var map;
    var motorMap;
    var network;
    var networkEdgeArray = [];
    var markers = [];
    var geocoder;
    var base_url = $("#base_url").val();
    var iconBase = base_url+'html/assets/icons/';
    var assignmentPolygon = "",drawingManager;
    var polygonLayer;
    var measureTool;
    var polyLine;
    var outlineMarkers = new Array();
    var markerArray = new Array();
    var searchMarker;
    var infowindow;
    var icons = {
        motor: {
            icon: {
                url: iconBase + 'motor.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Motor"
        },
        pole: {
            icon: {
                url: iconBase + 'pole.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(15, 15),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(7, 7)
            },
            name: "Pole"
        },
        substation: {
            icon: {
                url: iconBase + 'substation.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(50, 73),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(52, 36)
            },
            name: "Substation"
        },
        controlPanel: {
            icon: {
                url: iconBase + 'controlPanel.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Control Panel"
        },
        deviceBank: {
            icon: {
                url: iconBase + 'deviceBank.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Device Bank"
        },
        tapPoint: {
            icon: {
                url: iconBase + 'tapPoint.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Tap Point"
        },
        transformerBank: {
            icon: {
                url: iconBase + 'transformerBank.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Transformer Bank"
        }
    };
    var motorIcons = {

    }
    /**************************************************** */
$(function(){
    initSearchBar();
    $("#substation").val($("#selectedSubstation").val());
	initMap();
    var motorLocation = {lat: parseFloat($("#motorY").val()), lng: parseFloat($("#motorX").val())};
    loadSubstationData(motorLocation);
    //on selecting a substation plot motor,poles and substation geometry on map and zoom to substation location
    $("#substation").on("change",function(){
        loadSubstationData();
    });

});
function initSearchBar()
{
    $.post(base_url+"mobile/getMotorApiList",function(data){
        $(".search-bar").fadeIn(300);
        console.log("api_no loaded");
        data = jQuery.parseJSON(data);
        var motorData = {};
        $.each(data,function(key,apiData){
            motorData[apiData.api_no] = null;
        });
        $('#searchField').autocomplete({
            data: motorData
        });
        $('#searchField').keypress(function(e) {
            if(e.which == 13) {
                getApiDetails();
            }
        });
    });
    $("#searchBtn").on("click",function(){
        getApiDetails();
    });
}
function getApiDetails()
{
    $(".search-bar .progress").fadeIn(300);
    var apiID = $("#searchField").val();
    if(apiID!="")
    {
        $.post(base_url+"mobile/getMotorApiDetail/"+apiID,function(data){
            $(".search-bar .progress").fadeOut(300);
            var motorData = jQuery.parseJSON(data);
            var location = {lat: parseFloat(motorData.y), lng: parseFloat(motorData.x)};
            map.setCenter(location);
            locateMotor(motorData);
            map.setZoom(15);
        });
    }
}

function locateMotor(motorData)
{
    if(searchMarker != undefined)
    {
        searchMarker.setMap(null);
    }
    var marker = new google.maps.Marker({
        position: {lat: parseFloat(motorData.y), lng: parseFloat(motorData.x)},
        icon: icons.motor.icon,
        map: map,
        ogc_fid: motorData.ogc_fid
    });
    searchMarker = marker;
    marker.addListener('click', function() {
        showMapOverlay();
        getMotorData(marker.ogc_fid);
    });
}


function loadSubstationData(motorLocation = "")
{
    showMapOverlay();
    var substation = $("#substation option:selected");
    var location = {lat: substation.data("lat"), lng: substation.data("lng")}
    var title = substation.text();
    var substationID = substation.val();
    plotGeom(substationID);
    if(motorLocation != "")
    {
        if(infowindow){ infowindow.close(); }
        map.setCenter(motorLocation);
        map.setZoom(19);
        var contentString = '<b>Motor</b>';
        infowindow = new google.maps.InfoWindow({
            content: contentString,
            position: motorLocation
        });
        infowindow.open(map);
    }
    else
    {
        map.setCenter(location);
        map.setZoom(15);
    }

}
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
    	center: {lat: 38.89511, lng: -77.03637},
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            position: google.maps.ControlPosition.BOTTOM_CENTER
        }
    });
}


function plotGeom(substationID)
{
    var base_url = $("#base_url").val();
    $.post(base_url+"mobile/getSubstationGeom/"+substationID+"/",function(data){
        data = jQuery.parseJSON(data);
        var location = {lat: parseFloat(data.substation.y), lng: parseFloat(data.substation.x)};
        var title = data.substation.name;
        addSubstation(substationID,location, title);
        plotMotors(data.motors);
        plotPoles(data.poles);
        plotOHlines(data.ohlines);
    });
}
function plotMotors(motors)
{
    $.each(motors,function(motorIndex,motorObject){
        var motorGeoJSON = $.geo.WKT.parse(motorObject.motorwkt);
        var marker = new google.maps.Marker({
            position: {lat: motorGeoJSON.coordinates[1], lng: motorGeoJSON.coordinates[0]},
            icon: icons.motor.icon,
            map: map,
            ogc_fid: motorObject.ogc_fid,
            api_no: motorObject.api_no,
            api_alphanumber: motorObject.api_alphanumber
        });
        marker.addListener('click', function() {
            showMapOverlay();
            getMotorData(marker.ogc_fid);
            if(infowindow){ infowindow.close(); }
            var contentString = '<b>Motor</b>';
            infowindow = new google.maps.InfoWindow({
                content: contentString,
                position: marker.position
            });
            infowindow.open(map);
            //infowindow.open(map, marker);
            //getMotorDetails(marker.api_no);
        });
    });
}
function plotPoles(poles)
{
    $.each(poles,function(poleIndex,poleObject){
        var poleGeoJSON = $.geo.WKT.parse(poleObject.polewkt);
        var marker = new google.maps.Marker({
            position: {lat: poleGeoJSON.coordinates[1], lng: poleGeoJSON.coordinates[0]},
            icon: icons.pole.icon,
            map: map,
            ogc_fid: poleObject.ogc_fid
        });
        marker.addListener('click', function() {
            showMapOverlay();
            getPoleData(marker.ogc_fid);
        });
    });
}
    function getPoleData(ogc_fid)
    {
        $("#objectDetail").fadeIn(300);
        $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
        $.post(base_url+"mobile/getPoleData/"+ogc_fid,function(data){
            data = jQuery.parseJSON(data);
            showPoleData(data);
        });
    }

    function showPoleData(data)
    {
        var title = "Pole";
        var dataArray = {
            "Comment" : data.comments,
            "Pole Type" : data.poletype,
            "Substation" : data.s_substation,
            "Subsection" : data.s_subsection,
            "Datasource" : data.data_source,
            "Link" : data.html_link
        }
        updateObjectInfowindow(title,dataArray)
    }
function plotOHlines(ohlines)
{
    console.log(ohlines);
    $.post(base_url+"mobile/getFedderColors",function(fedderColors){
        fedderColors = jQuery.parseJSON(fedderColors);
        var primaryFeature = [];
        $.each(ohlines.primary,function(lineIndex,lineObject){
            var lineGeoJSON = $.geo.WKT.parse(lineObject.primarywkt);
            primaryFeature.push({
                "type": "Feature",
                "geometry": lineGeoJSON,
                "properties": {
                    "color" : fedderColors[lineObject.s_subsection],
                    "type": "ohprimaryline",
                    "ogc_fid": lineObject.ogc_fid
                }
            });
        });
        map.data.addGeoJson({
            "type": "FeatureCollection",
            "features": primaryFeature
        });

        var secondaryFeature = [];
        $.each(ohlines.secondary,function(lineIndex,lineObject){
            var lineGeoJSON = $.geo.WKT.parse(lineObject.secondarywkt);
            secondaryFeature.push({
                "type": "Feature",
                "geometry": lineGeoJSON,
                "properties": {
                    "color" : fedderColors[lineObject.s_subsection],
                    "type": "ohsecondaryline",
                    "ogc_fid": lineObject.ogc_fid
                }
            });
        });
        map.data.addGeoJson({
            "type": "FeatureCollection",
            "features": secondaryFeature
        }); 

        map.data.addListener('click', function(event) {
            showMapOverlay();
            var ogc_fid = event.feature.getProperty('ogc_fid');
            var lineTable = event.feature.getProperty('type');
            console.log(ogc_fid,lineTable);
            getLineData(ogc_fid,lineTable);
        });



        hideMapOverlay();
    });
}

    function getLineData(ogc_fid,table)
    {
        $("#objectDetail").fadeIn(300);
        $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
        $.post(base_url+"mobile/getLineData/",{ogc_fid:ogc_fid,table:table},function(data){
            data = jQuery.parseJSON(data);
            showLineData(data,table);
        });
    }

    function showLineData(data,table)
    {
        if(table=="ohprimaryline")
        {
            var title = "OH Primary Line";
        }
        else
        {
            var title = "OH Secondary Line";
        }
        
        var dataArray = {
            "Comment" : data.comments,
            "Utility" : data.utility,
            "Substation" : data.s_substation,
            "Subsection" : data.s_subsection,
            "Upload Date" : data.s_uploaddate
        }
        updateObjectInfowindow(title,dataArray)
    }



function getMotorData(ogc_fid)
{
    $("#objectDetail").fadeIn(300);
    $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
    $.post(base_url+"mobile/getMotorData/"+ogc_fid,function(data){
        data = jQuery.parseJSON(data);
        showMotorData(data);
    });
}
function showMotorData(data)
{
    var title = "Motor";
    var dataArray = {
        "Label" : data.label,
        "HP" : data.hp,
        "PF" : data.pf,
        "EFF" : data.eff,
        "Motor Type" : data.motortype,
        "Manufacturer" : data.manufacturer,
        "Model Number" : data.modelnumber,
        "Serial Number" : data.serialnumber,
        "RPM" : data.rpm,
        "Service Factor" : data.servicefactor,
        "NEMA Type" : data.nematype,
        "NEMA Design" : data.nemadesign,
        "RTD Volts" : data.rtdvolts,
        "Rated AMPs" : data.ratedamps,
        "Encloser" : data.enclosure,
        "API NO" : data.api_no,
        "Link" : data.html_link,
        "Diagram" : data.api_alphanumber
    }
    updateObjectInfowindow(title,dataArray)
}
function addSubstation(substationID,location, title) {
    var marker = new google.maps.Marker({
        position: location,
        icon: icons.substation.icon,
        map: map,
        title: title,
        substationID: substationID
    });
    marker.addListener('click', function() {
        getSubstationData(marker.substationID);
    });
    markers.push(marker);
}
function getSubstationData(substationID)
{
    $("#objectDetail").fadeIn(300);
    $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
    $.post(base_url+"mobile/getSubstationData/"+substationID,function(data){
        data = jQuery.parseJSON(data);
        showSubstationData(data);
    });
}
function showSubstationData(data)
{
    var title = data.name;
    var dataArray = {
        "Comment" : data.comments,
        "utility" : data.utility,
        "Contract" : data.oxy_contact,
        "Owner" : data.owner,
        "Oxy Region" : data.oxyregion,
        "Link" : data.html_link,
    }
    updateObjectInfowindow(title,dataArray)
}


function updateObjectInfowindow(title,data)
{
        var contentHtml = '<div class="object-title">'+title+'</div>';
        $.each(data,function(attribute,value){
            if(attribute!="Link" && attribute!="Diagram")
            {
                if(value!=null)
                {
                    contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span>'+value+'</div>';
                }
                
            }
            else if(value!=null)
            {
                if(attribute == "Link")
                {
                    if (value.indexOf("http") >= 0)
                    {
                        contentHtml = contentHtml+'<div class="object-row"><a class="btn-full waves-effect waves-light btn blue darken-2" href="'+value+'" target="_blank">Open Link</a></div>';
                    } 
                }
                else
                {
                    var mobileLink = base_url+"mobile/motordiagram/"+value;
                    contentHtml = contentHtml+'<div class="object-row"><a class="btn-full waves-effect waves-light btn blue darken-2" href="'+mobileLink+'" target="_blank">View Diagram</a>';
                }
            }
            
        });
        $("#featureDetailModal #detailContainer").html(contentHtml);
        hideMapOverlay();
        $("#featureDetailModal").openModal();
}

function hideMapOverlay()
{
    $(".map-overlay").fadeOut(300);
}

function showMapOverlay()
{
    $(".map-overlay").fadeIn(300);
}