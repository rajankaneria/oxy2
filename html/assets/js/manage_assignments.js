$(document).ready(function(){
    initKendoGrid();
    $('select').material_select();
    $("#openAssignment").on("click",function(data){
        var base_url = $("#base_url").val();
        var assignmentID;
        if($("#grid .k-state-selected").length != 0)
        {
            var grid = $("#grid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            assignmentID = selectedItem.id;
        }
        if(assignmentID == undefined)
        {
            $('#assignmentMsg').openModal();
        }
        else
        {
            window.location.href = base_url+"assignments/details/"+assignmentID+"/";
        }
    });

    $("#deleteAssignment").on("click",function(){
        initDeleteAssignment();
    });

    $("#confirmDeleteAssignmentBtn").on("click",function(){
        deleteAssignment();
    });

    $("#reassignCrewBtn").on("click",function(){
        var assignmentID;
        if($("#grid .k-state-selected").length != 0)
        {
            var grid = $("#grid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            assignmentID = selectedItem.id;
        }
        if(assignmentID == undefined)
        {
            $('#assignmentMsg').openModal();
        }
        else
        {
            $("#reAssignModal").openModal();
            $("#crewReassign").val(selectedItem.crew);
        } 
    });

    $("#saveCrewBtn").on("click",function(){
        reAssignCrew();
    }); 
});

function initKendoGrid()
{
    var base_url = $("#base_url").val();
    $.post(base_url+"assignments/assignmentJson/",function(data){
        data = jQuery.parseJSON(data);
        console.log(data);
            $("#grid").kendoGrid({
				toolbar: ["excel"],
				excel: {
	                fileName: "Assignments.xlsx",
	                filterable: true
	            },
			    scrollable: false,
			    sortable: true,
                selectable: true,
                columns: [{
                    field: "id",
                    title: "ID",
                    width: "5%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "assignmentid",
                    title: "Assignment ID",
                    width: "13%",
                    attributes: {
                        style: "text-align: left;"
                    }
                },{
                    field: "type",
                    title: "Type",
                    width: "7%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "priority",
                    title: "Priority",
                    width: "7%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "assigndate",
                    title: "Assign Date",
                    width: "8%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "expecteddate",
                    title: "Expected Date",
                    width: "8%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "duration",
                    title: "Duration",
                    width: "5%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "status",
                    title: "Status",
                    width: "8%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "description",
                    title: "Description",
                    width: "20%",
                    attributes: {
                        style: "text-align: left;"
                    }
                },{
                    field: "name",
                    title: "Substation",
                    width: "10%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "crewname",
                    title: "Crew",
                    width: "5%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "newChatCount",
                    title: "New Chat",
                    width: "4%",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field:"crew",
                    hidden:true
                }],
                dataSource:data
            });

            /*
            var tdCounter = 1;
            $(".k-grid-header-wrap th").each(function(){
                $(this).width($(".k-grid-content tr td:nth-child("+tdCounter+")").width());
                tdCounter++;
            });
            */
    });
}

function initDeleteAssignment()
{
    var assignmentID;
        if($("#grid .k-state-selected").length != 0)
        {
            var grid = $("#grid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            assignmentID = selectedItem.id;
        }
    if(assignmentID == undefined)
    {
        $('#assignmentMsg').openModal();
    }
    else
    {
        $("#deleteConfirmMsg").openModal();
    }    
}

function deleteAssignment()
{
    $("#deleteConfirmMsg .progress").fadeIn(300);
    var base_url = $("#base_url").val();
    var grid = $("#grid").data("kendoGrid");
    var selectedItem = grid.dataItem(grid.select());
    var assignmentID = selectedItem.id;
    $.post(base_url+"assignments/deleteAssignment/"+assignmentID+"/",function(data){
        $('#grid').data("kendoGrid").dataSource.remove(selectedItem);
        $('#grid').data("kendoGrid").dataSource.sync();
        $('#grid').data('kendoGrid').refresh();
        $("#deleteConfirmMsg .progress").fadeOut(300);
        $("#deleteConfirmMsg").closeModal();
    });
}

function reAssignCrew()
{
    $("#reAssignModal .progress").fadeIn(300);
    var base_url = $("#base_url").val();
    var grid = $("#grid").data("kendoGrid");
    var selectedItem = grid.dataItem(grid.select());
    var selectedRow = grid.select();
    var selectedRowIndex = selectedRow.index();
    var assignmentID = selectedItem.id;
    var uid = selectedItem.uid;
    var crewID = $("#crewReassign option:selected").val();
    var crewName = $("#crewReassign option:selected").text();
    $.post(base_url+"assignments/reAssignCrew/"+assignmentID+"/",{crewID:crewID},function(data){
        $("tr.assignment-row#assignment"+assignmentID+" #crewName").html(crewName);

        $('#grid').data("kendoGrid").dataSource.data()[selectedRowIndex].crewname = crewName;
        $('#grid').data("kendoGrid").dataSource.data()[selectedRowIndex].crew = crewID;
        $('#grid').data("kendoGrid").dataSource.sync();
        $('#grid').data('kendoGrid').refresh();
        selectGridRow(assignmentID);
        $("#reAssignModal .progress").fadeOut(300);
        $("#reAssignModal").closeModal();
    });
}
