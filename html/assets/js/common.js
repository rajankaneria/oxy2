
    /* ------------- Constant Declaration------------- */
    var map;
    var motorMap;
    var network;
    var networkEdgeArray = [];
    var markers = [];
    var geocoder;
    var base_url = $("#base_url").val();
    var iconBase = base_url+'html/assets/icons/';
    var assignmentPolygon = "",drawingManager;
    var polygonLayer;
    var measureTool;
    var polyLine;
    var outlineMarkers = new Array();
    var markerArray = new Array();
    var searchMarker;
    var icons = {
        motor: {
            icon: {
                url: iconBase + 'motor.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Motor"
        },
        pole: {
            icon: {
                url: iconBase + 'pole.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(15, 15),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(7, 7)
            },
            name: "Pole"
        },
        substation: {
            icon: {
                url: iconBase + 'substation.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(50, 73),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(52, 36)
            },
            name: "Substation"
        },
        controlPanel: {
            icon: {
                url: iconBase + 'controlPanel.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Control Panel"
        },
        deviceBank: {
            icon: {
                url: iconBase + 'deviceBank.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Device Bank"
        },
        tapPoint: {
            icon: {
                url: iconBase + 'tapPoint.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Tap Point"
        },
        transformerBank: {
            icon: {
                url: iconBase + 'transformerBank.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(22, 22),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(11, 11)
            },
            name: "Transformer Bank"
        }
    };
    var motorIcons = {

    }
    /**************************************************** */
function hideMapOverlay()
{
    $(".map-overlay").fadeOut(300);
}

function showMapOverlay()
{
    $(".map-overlay").fadeIn(300);
}
function plotMotorDiagram(api_no)
{
    var base_url = $("#base_url").val();
    var canvas = document.getElementById('myCanvas');
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    $(".motor-map-overlay").fadeIn();
    $.post(base_url+"database/motorDiagram/"+api_no,function(data){
        var canvasPoints = $.parseJSON(data);
        var featuresIcon = {
            "controlpanel": icons.controlPanel.icon,
            "overcurrentdevicebank": icons.deviceBank.icon,
            "tappoint": icons.tapPoint.icon,
            "transformerbank": icons.transformerBank.icon
        };
        $.each(canvasPoints,function(dataIndex,dataObject){
            var imageObj = new Image();
            imageObj.onload = function(){
                context.drawImage(imageObj,dataObject.x,dataObject.y)
            }
            imageObj.src = featuresIcon[dataIndex].url;  
        });
        context.beginPath();
        var pointCount = 0;
        $.each(canvasPoints,function(dataIndex,dataObject){
            if(pointCount == 0){ 
                context.moveTo(dataObject.x + 11, dataObject.y + 11); 
            }
            else
            {
                context.lineTo(dataObject.x + 11, dataObject.y + 11);
            }
            pointCount++;
        });
        context.stroke();
        $(".motor-map-overlay").fadeOut();
    });
}
function plotGeom(substationID)
{
    var base_url = $("#base_url").val();
    $.post(base_url+"assignments/getSubstationGeom/"+substationID+"/",function(data){
        data = jQuery.parseJSON(data);
        var location = {lat: parseFloat(data.substation.y), lng: parseFloat(data.substation.x)};
        var title = data.substation.name;
        addSubstation(substationID,location, title);
        plotMotors(data.motors);
        plotPoles(data.poles);
        plotOHlines(data.ohlines);
    });
}
function plotMotors(motors)
{
    console.log("plotMotors");
    $.each(motors,function(motorIndex,motorObject){
        var motorGeoJSON = $.geo.WKT.parse(motorObject.motorwkt);
        var marker = new google.maps.Marker({
            position: {lat: motorGeoJSON.coordinates[1], lng: motorGeoJSON.coordinates[0]},
            icon: icons.motor.icon,
            map: map,
            ogc_fid: motorObject.ogc_fid,
            api_no: motorObject.api_no,
            api_alphanumber: motorObject.api_alphanumber
        });
        marker.addListener('click', function() {
            getMotorData(marker.ogc_fid);
            //infowindow.open(map, marker);
            //getMotorDetails(marker.api_no);
        });
    });
}
function getMotorDetails(api_alphanumber)
{
    $("#featureDetail").hide();
    $("#motorModal").openModal();
    var base_url = $("#base_url").val();
    $("#motorDiagramContainer").html("");
    $(".motor-map-overlay").fadeIn();
    $.post(base_url+"database/getMotorEdges/"+api_alphanumber,function(data){
        var canvasPoints = $.parseJSON(data);
        var nodeDataSet = [];
        var edgeDataSet = [];
        var nodeIds = [];
        var DIR = base_url+"html/assets/icons/motorDiagram/";
        $.each(canvasPoints.nodeList,function(dataIndex,dataObject){
                var currentNode = {
                    id: dataIndex,
                    label: dataObject.label,
                    image: DIR + dataObject.tableName+'.png', 
                    shape: 'image'
                };
            nodeDataSet.push(currentNode);
        });

        //console.log(nodeDataSet);
        var lineColorArray = {
            "ohprimary" : {
                color: 'orange',
                width: 2,
                dashes:false
            },
            "ohsecondary" : {
                color: 'red',
                width: 2,
                dashes:false
            },
            "ugsecondary" : {
                color: 'orange',
                width: 2,
                dashes:true
            },
            "ugprimary" : {
                color: 'red',
                width: 2,
                dashes:true
            }
        };
        $.each(canvasPoints.lines,function(dataIndex,dataObject){

            $.each(dataObject,function(lineIndex,lineArray){
                edgeDataSet.push({from: lineArray["fromguid"], to: lineArray["toguid"],lineType:dataIndex, color: lineColorArray[dataIndex]["color"], width: lineColorArray[dataIndex]["width"], dashes: lineColorArray[dataIndex]["dashes"]});
            });
        });
        networkEdgeArray = edgeDataSet;

        var container = document.getElementById('motorDiagramContainer');
        var nodes = new vis.DataSet(nodeDataSet);
        var edges = new vis.DataSet(edgeDataSet);
        var data = {
            nodes: nodes,
            edges: edges
        };

        //networkEdgeArray = edges.data;


        /*
        var options = {
            layout: {
                hierarchical: {
                    enabled: true,
                    blockShifting: true,
                    edgeMinimization: true,
                    parentCentralization: true,
                    direction: 'LR',
                    sortMethod: 'hubsize'
                }
            },
            physics:{
                enabled:true
            }
        };
        */



        var options = {
            nodes: {
              scaling: {
                min: 16,
                max: 32
              },
              font:{
                color: "#000",
                size: 16
              }
            },
            edges: {
              color: 'grey',
              smooth: false,
              hoverWidth: 3
            },
            physics:{
              barnesHut:{gravitationalConstant:-30000},
              stabilization: {iterations:2500}
            },
            interaction:{
                selectConnectedEdges: false,
                hover: true
            }
          };


        network = new vis.Network(container, data, options);

        network.on('selectNode', function(p) {
            var selectedNodeId = p.nodes[0];
            var selectedElement = canvasPoints.nodeList[selectedNodeId].label;
            $("#featureDetail").hide();
            $(".motor-map-overlay").fadeIn();
            $.post(base_url+"database/getFeatureDetails/",{featureName:selectedElement,featureId:selectedNodeId},function(data){
                data = $.parseJSON(data);
                $("#featureDetail #detailContainer").html("");
                $("#featureDetail #detailContainer").append("<div class='motor-feature-title'>"+selectedElement+"</div>");
                $.each(data.fields,function(key,fieldRow){
                    var fieldLabel = fieldRow["field_label"];
                    var fieldName = fieldRow["field_name"];
                    var isLink = fieldRow["is_link"];

                    $("#featureDetail #detailContainer").append("<b>"+fieldLabel+"</b>: ");
                    if(isLink==0)
                    {
                        $("#featureDetail #detailContainer").append(data.data[fieldName]);
                    }
                    else
                    {
                        $("#featureDetail #detailContainer").append("<a href='"+data.data[fieldName]+"' target='_blank'>Click Here</a>");
                    }
                    
                    $("#featureDetail #detailContainer").append("<br/>");
                });
                $("#hideFeatureDetailBtn").on("click",function(){
                    $("#featureDetail").hide();
                    $("#featureDetail #detailContainer").html("");
                });
                $("#featureDetail").fadeIn(300);
                $(".motor-map-overlay").hide();
            });

        }); 
        
        network.on('selectEdge',function(q){
            var selectedEdgeId = q.edges[0];
            var selectedEdgeData = networkEdgeArray.filter(function ( obj ) {
                return obj.id === selectedEdgeId;
            })[0];
            $("#featureDetail").hide();
            $(".motor-map-overlay").fadeIn();
            $.post(base_url+"database/getLineDetails/",{lineType:selectedEdgeData.lineType,from:selectedEdgeData.from,to:selectedEdgeData.to},function(data){
                data = $.parseJSON(data);
                $("#featureDetail #detailContainer").html("");
                $("#featureDetail #detailContainer").append("<div class='motor-feature-title'>"+selectedEdgeData.lineType+"</div>");
                $.each(data.fields,function(key,fieldRow){
                    var fieldLabel = fieldRow["field_label"];
                    var fieldName = fieldRow["field_name"];
                    var isLink = fieldRow["is_link"];
                    $("#featureDetail #detailContainer").append("<b>"+fieldLabel+"</b>: ");
                    if(isLink==0)
                    {
                        $("#featureDetail #detailContainer").append(data.data[fieldName]);
                    }
                    else
                    {
                        $("#featureDetail #detailContainer").append("<a href='"+data.data[fieldName]+"' target='_blank'>Click Here</a>");
                    }
                    
                    $("#featureDetail #detailContainer").append("<br/>");
                });
                $("#hideFeatureDetailBtn").on("click",function(){
                    $("#featureDetail").hide();
                    $("#featureDetail #detailContainer").html("");
                });

                $("#featureDetail").fadeIn(300);
                $(".motor-map-overlay").hide();
            });
        });
        

        $(".motor-map-overlay").fadeOut();
    });

}
function plotPoles(poles)
{
    $.each(poles,function(poleIndex,poleObject){
        var poleGeoJSON = $.geo.WKT.parse(poleObject.polewkt);
        var marker = new google.maps.Marker({
            position: {lat: poleGeoJSON.coordinates[1], lng: poleGeoJSON.coordinates[0]},
            icon: icons.pole.icon,
            map: map,
            ogc_fid: poleObject.ogc_fid
        });
        marker.addListener('click', function() {
            getPoleData(marker.ogc_fid);
        });
    });
}
function plotOHlines(ohlines)
{
    console.log(ohlines);
    $.post(base_url+"assignments/getFedderColors",function(fedderColors){
        fedderColors = jQuery.parseJSON(fedderColors);
        var primaryFeature = [];
        $.each(ohlines.primary,function(lineIndex,lineObject){
            var lineGeoJSON = $.geo.WKT.parse(lineObject.primarywkt);
            primaryFeature.push({
                "type": "Feature",
                "geometry": lineGeoJSON,
                "properties": {
                    "color" : fedderColors[lineObject.s_subsection],
                    "type": "ohprimaryline",
                    "ogc_fid": lineObject.ogc_fid
                }
            });
        });
        map.data.addGeoJson({
            "type": "FeatureCollection",
            "features": primaryFeature
        });

        var secondaryFeature = [];
        $.each(ohlines.secondary,function(lineIndex,lineObject){
            var lineGeoJSON = $.geo.WKT.parse(lineObject.secondarywkt);
            secondaryFeature.push({
                "type": "Feature",
                "geometry": lineGeoJSON,
                "properties": {
                    "color" : fedderColors[lineObject.s_subsection],
                    "type": "ohsecondaryline",
                    "ogc_fid": lineObject.ogc_fid
                }
            });
        });
        map.data.addGeoJson({
            "type": "FeatureCollection",
            "features": secondaryFeature
        }); 

        map.data.addListener('click', function(event) {
            var ogc_fid = event.feature.getProperty('ogc_fid');
            var lineTable = event.feature.getProperty('type');
            console.log(ogc_fid,lineTable);
            getLineData(ogc_fid,lineTable);
        });



        hideMapOverlay();
    });
}

    function getLineData(ogc_fid,table)
    {
        $("#objectDetail").fadeIn(300);
        $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
        $.post(base_url+"assignments/getLineData/",{ogc_fid:ogc_fid,table:table},function(data){
            data = jQuery.parseJSON(data);
            showLineData(data,table);
        });
    }

    function showLineData(data,table)
    {
        if(table=="ohprimaryline")
        {
            var title = "OH Primary Line";
        }
        else
        {
            var title = "OH Secondary Line";
        }
        
        var dataArray = {
            "Comment" : data.comments,
            "Utility" : data.utility,
            "Substation" : data.s_substation,
            "Subsection" : data.s_subsection,
            "Upload Date" : data.s_uploaddate
        }
        updateObjectInfowindow(title,dataArray)
    }

function addSubstation(substationID,location, title) {

    var marker = new google.maps.Marker({
        position: location,
        icon: icons.substation.icon,
        map: map,
        title: title,
        substationID: substationID
    });

    marker.addListener('click', function() {
        getSubstationData(marker.substationID);
    });
    markers.push(marker);
}

function initMotorMap(){
    motorMap = new google.maps.Map(document.getElementById('motorMap'), {
        center: {lat: 38.89511, lng: -77.03637},
        zoom: 8
    });
}


    function updateObjectInfowindow(title,data)
    {
        console.log("update object info window");
        var contentHtml = '<div class="object-title">'+title+'</div>';
        $.each(data,function(attribute,value){
            if(attribute!="Link" && attribute!="Diagram")
            {
                if(value!=null)
                {
                    contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span>'+value+'</div>';
                }
                
            }
            else if(value!=null)
            {
                if(attribute == "Link")
                {
                    if (value.indexOf("http") >= 0)
                    {
                        contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span><a href="'+value+'" target="_blank">Click Here</a></div>';
                    } 
                }
                else
                {
                    contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span><a href="#!" onclick="getMotorDetails(\''+value+'\')">Click Here</a>';
                    contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">QR Code: </span><a href="#!" onclick="showQRCode(\''+value+'\')">Click Here</a>';
                }
            }
            
        });
        $("#objectDetail #mainContent").html(contentHtml);
    }
    function getMotorData(ogc_fid)
    {
        $("#objectDetail").fadeIn(300);
        $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
        $.post(base_url+"assignments/getMotorData/"+ogc_fid,function(data){
            data = jQuery.parseJSON(data);
            showMotorData(data);
        });
    }
    function showQRCode(api_alphanumber)
    {
        $("#qrModal").openModal();
        $("#qrCode").html("");
        var qrText = base_url+"mobile/motordiagram/"+api_alphanumber;
        var qrOptions = {
            render: 'canvas',
            minVersion: 1,
            maxVersion: 40,
            ecLevel: 'L',
            left: 0,
            top: 0,
            size: $("#qrCode").width(),
            fill: '#000',
            background: null,
            text: qrText,
            radius: 0,
            quiet: 0,
            mode: 0,
            mSize: 0.1,
            mPosX: 0.5,
            mPosY: 0.5,
            label: 'no label',
            fontname: 'sans',
            fontcolor: '#000',
            image: null
        };
        $("#qrCode").qrcode(qrOptions);
        $("#qrCodeLink").html(qrText);
    }
    function showMotorData(data)
    {
        var title = "Motor";
        var dataArray = {
            "Label" : data.label,
            "HP" : data.hp,
            "PF" : data.pf,
            "EFF" : data.eff,
            "Motor Type" : data.motortype,
            "Manufacturer" : data.manufacturer,
            "Model Number" : data.modelnumber,
            "Serial Number" : data.serialnumber,
            "RPM" : data.rpm,
            "Service Factor" : data.servicefactor,
            "NEMA Type" : data.nematype,
            "NEMA Design" : data.nemadesign,
            "RTD Volts" : data.rtdvolts,
            "Rated AMPs" : data.ratedamps,
            "Encloser" : data.enclosure,
            "API NO" : data.api_no,
            "Link" : data.html_link,
            "Diagram" : data.api_alphanumber
        }
        updateObjectInfowindow(title,dataArray)
    }