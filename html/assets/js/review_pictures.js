var kendoGridData;
$(function(){
    initKendoGrid();
    $("#reviewPicture").on("click",function(){
        var fileName;
        if($("#grid .k-state-selected").length != 0)
        {
            var grid = $("#grid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            fileName = selectedItem.name;
            console.log(selectedItem);
        }
        if(fileName == undefined)
        {
            $('#pictureMsg').openModal();
        }
        else
        {
            $('#reviewModal').openModal();
            var selectedData = $.grep(kendoGridData, function(v,i) {
                return v["fid"] === selectedItem.fid;
            });
            console.log(selectedData);
            $("#currentImageIndex").val($.inArray(selectedData[0],kendoGridData));
            loadPicture(selectedItem);      
        }
    });

    $("#addPicture").on("click",function(){
        $('#addPictureModal').openModal();
    });

    $("#rotateLeft").on("click",function(){
        rotateLeft();
    });
    $("#rotateRight").on("click",function(){
        rotateRight();
    });

    $("#nextImage").on("click",function(){
        nextImage();
    });
    $("#previousImage").on("click",function(){
        previousImage();
    });
    $("#getImage").on("click",function(){
        var imageSrc = $("#imageUrl").val();
        $("#saveImage").removeClass("hide");
        $('#imageContainer').html('<img data-angle="0" class="review-image responsive-img" src="'+imageSrc+'" />');
    });
    $("#saveImage").on("click",function(){
        saveImage();
    });
    $("#assignUser").on("click",function(){
        var fileName;
        if($("#grid .k-state-selected").length != 0)
        {
            var grid = $("#grid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            fileName = selectedItem.name;
            console.log(selectedItem);
        }
        if(fileName == undefined)
        {
            $('#pictureMsg').openModal();
        }
        else
        {
            $('#assignUserModal').openModal(); 
            $("#userList").val(selectedItem.reviewedby);
        }
    });
    $("#assignUserBtn").on("click",function(){
        var grid = $("#grid").data("kendoGrid");
        var selectedItems = grid.dataItems(grid.select());
        var userID = $("#userList option:selected").val();


        grid.select().each(function() {
            var dataItem = grid.dataItem($(this));
            assignUser(dataItem.fid,userID);
        });

        
    });
});
function assignUser(fid,userID)
{
    var base_url = $("#base_url").val();
    $("#assignUserModal .progress").fadeIn(300);
    $("#assignUserBtn").attr("disabled","disabled");
    $.post(base_url+"pictures/assignUser",{fid:fid,userID:userID},function(data){
        $("#assignUserBtn").removeAttr("disabled");
        $("#assignUserModal .progress").hide();
        $('#assignUserModal').closeModal(); 
        data = $.parseJSON(data);
        updateRow(fid,data);
    });
}
function nextImage()
{
    var oldIndex = $("#currentImageIndex").val();
    var newIndex = parseInt(oldIndex) + 1;
    if(newIndex >= kendoGridData.length){ newIndex = 0; }
    //console.log(kendoGridData[newIndex]);
    $("#currentImageIndex").val(newIndex);
    saveCurrentImage(kendoGridData[oldIndex]);
    changeImage(kendoGridData[newIndex]);
}
function previousImage()
{
    var oldIndex = $("#currentImageIndex").val();
    var newIndex = parseInt(oldIndex) - 1;
    if(newIndex < 0){ newIndex = kendoGridData.length - 1; }
    //console.log(kendoGridData[newIndex]);
    $("#currentImageIndex").val(newIndex);
    saveCurrentImage(kendoGridData[oldIndex]);
    changeImage(kendoGridData[newIndex]);
}
function saveCurrentImage(imageObject)
{
    var base_url = $("#base_url").val();
    var angle = parseInt($("#pictureContainer img").data("angle")); 
    var fileName = imageObject.name;
    var fid = imageObject.fid;



    if(imageObject.rotation == null){ imageObject.rotation = 0; }

    $("#previousImage").attr("disabled","disabled");
    $("#nextImage").attr("disabled","disabled");
    $.post(base_url+"pictures/rotateImage",{rotation:angle,fid:fid},function(data){
        $("#previousImage").removeAttr("disabled");
        $("#nextImage").removeAttr("disabled");
        //initKendoGrid();
        console.log(data);
        data = $.parseJSON(data);
        updateRow(fid,data);
    });
 
}

function updateRow(fid,updatedData)
{
    var uid;
    var grid = $("#grid").data("kendoGrid");
    $('#grid tbody tr').each(function(){
        if($(this).find('td').eq(0).text() == fid){
            uid = $(this).data("uid");
            var row = grid.table.find("[data-uid="+uid+"]");
            grid.select(row);
            $.each(updatedData,function(key,value){
                var columnIndex = grid.wrapper.find(".k-grid-header [data-field="+key+"]").index();
                $("[data-uid="+uid+"] td").eq(columnIndex).text(value);
            });
        }
    });

    /*
    var dataSource = grid.dataSource;
    var locationColumnIndex = grid.wrapper.find(".k-grid-header [data-field=location]).index();
    */

}

function changeImage(imageObject)
{
    var base_url = $("#base_url").val();
    $("#pictureContainer").html("");
    var currentTimestamp = $.now();
    var imageSrc = imageObject.name+"?"+currentTimestamp;
    if(imageObject.rotation == null){ imageObject.rotation = 0; }
    $('#pictureContainer').html('<img data-angle="0" class="review-image responsive-img" src="'+imageSrc+'" />');


}
function rotateLeft()
{
    var angle = $("#pictureContainer img").data("angle");
    $("#pictureContainer img").removeClass("image-rotate-"+angle);
    if(angle==0)
    {
        angle = 360;
    }
    var newAngle = angle - 90;
    $("#pictureContainer img").addClass("image-rotate-"+newAngle);
    $("#pictureContainer img").data("angle",newAngle);
    addTransformOrigin();
}

function rotateRight()
{
    var angle = $("#pictureContainer img").data("angle");
    $("#pictureContainer img").removeClass("image-rotate-"+angle);
    if(angle==360)
    {
        angle = 0;
    }
    var newAngle = angle + 90;
    $("#pictureContainer img").addClass("image-rotate-"+newAngle);
    $("#pictureContainer img").data("angle",newAngle);
    addTransformOrigin();
}

function saveRotation()
{
    var base_url = $("#base_url").val();
    var angle = parseInt($("#pictureContainer img").data("angle")); 
    var grid = $("#grid").data("kendoGrid");
    var selectedItem = grid.dataItem(grid.select());
    var fileName = selectedItem.picture;
    var fid = selectedItem.fid;
    $("#saveRotation").html("Saving...");
    $.post(base_url+"pictures/rotateImage",{rotation:angle,fid:fid},function(data){
        initKendoGrid();
    });
}
/*
function saveImage()
{
    var base_url = $("#base_url").val();
    var imageSrc = $("#imageUrl").val();
    var api_no = $("#api_no").val();
    if(api_no == "")
    {
        $("#addPictureModal .error-msg").fadeIn(300);
    }
    else
    {
        $("#saveImage").html("Saving...");
        $.post(base_url+"pictures/saveImage/",{imageSrc:imageSrc,api_no:api_no},function(data){
            $("#saveImage").html("Save");
            $("#imageUrl").val("");
            $("#api_no").val("");
            $("#saveImage").addClass("hide");
            $('#imageContainer').html('');
            initKendoGrid();
            $('#addPictureModal').closeModal();
        });
    }
}
*/
function addTransformOrigin()
{
    var imageWidth = $("#pictureContainer .review-image").width()/2;
    var imageHeight = $("#pictureContainer .review-image").height()/2;
    var xPosSTR = imageWidth+'px';
    var yPosSTR = imageHeight+'px';
    $('.review-image').css({
        'transform-origin':         '' + xPosSTR + ' ' + yPosSTR + ' 0px',
        '-webkit-transform-origin': '' + xPosSTR + ' ' + yPosSTR + ' 0px'
    });

}
function loadPicture(imageObject)
{
    var base_url = $("#base_url").val();
    $("#pictureContainer").html("");
    var currentTimestamp = $.now();
    var imageSrc = imageObject.name+"?"+currentTimestamp;

    if(imageObject.rotation == null){ imageObject.rotation = 0; }
    $('#pictureContainer').html('<img data-angle="0" class="review-image responsive-img" src="'+imageSrc+'" />');
    var currentIndex = $("#currentImageIndex").val();
    saveCurrentImage(kendoGridData[currentIndex]);
}
function calibrateImageStyle()
{
    var containerWidth = $("#pictureContainer").width();
    var containerHeight = $("#pictureContainer").height();
    if(containerHeight > containerWidth)
    {
        $("#pictureContainer").height(containerWidth);
    }
    else
    {
        $("#pictureContainer").width(containerHeight);

    }

}

function initKendoGrid()
{
    $("#selectAllGridRow").hide();
    $("#selectAllGridRow").off("click");
    $("#selectAllGridRow").on("click",function(){
        $("tbody tr").addClass("k-state-selected");
        $("tbody tr input.checkbox").prop('checked', true);
    });
    var base_url = $("#base_url").val();
    $.post(base_url+"pictures/getPictureList/",function(data){
        data = jQuery.parseJSON(data);
        kendoGridData = data;
        console.log(data);
            $("#grid").kendoGrid({
                scrollable: false,
			    sortable: true,
                selectable: "multiple, row",
                columns: [{
                    field: "fid",
                    title: "FID",
                    attributes: {
                        style: "text-align: center;"
                    },
                    template: '<input class="checkbox filled-in" type="checkbox" id="#: fid #" /><label class="checkbox-label" for="#: fid #">#: fid #</label>'
                },{
                    field: "pictype",
                    title: "Picture Type",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "featuretable",
                    title: "Feature",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "comments",
                    title: "Comments"
                },{
                    field: "reviewername",
                    title: "Reviewer UserID",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "reviewdatetime",
                    title: "Review Date",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "name",
                    title: "Picture Path",
                    hidden: true
                },{
                    field: "rotation",
                    title: "Rotation",
                    hidden: true
                }],
                dataSource:data,
                pageable:{
                    pageSize: 100,
                },
                change: function(e) {
                  $('tr').find('[type=checkbox]').prop('checked', false);
                  $('tr.k-state-selected').find('[type=checkbox]').prop('checked', true);
                },
                dataBound: function () {
                    $(".checkbox-label").on("click",function(e){
                        e.preventDefault();
                    });
                    $(".checkbox").bind("change", function (e) {
                        console.log(e);
                        $(e.target).closest("tr").toggleClass("k-state-selected");
                    });
                    
                }
            });
            $("#selectAllGridRow").show();
            /*
            var tdCounter = 1;
            $(".k-grid-header-wrap th").each(function(){
                $(this).width($(".k-grid-content tr td:nth-child("+tdCounter+")").width());
                tdCounter++;
            });
            */
    });
}