
        $(function(){
            initMap();
            initSearchBar();
        });

        function initSearchBar()
        {
            $.post(base_url+"assignments/getMotorApiList",function(data){
                $(".search-bar").fadeIn(300);
                console.log("api_no loaded");
                data = jQuery.parseJSON(data);
                var motorData = {};
                $.each(data,function(key,apiData){
                    motorData[apiData.api_no] = null;
                });
                $('#searchField').autocomplete({
                    data: motorData
                });
                $('#searchField').keypress(function(e) {
                    if(e.which == 13) {
                        getApiDetails();
                    }
                });
            });
            $("#searchBtn").on("click",function(){
                getApiDetails();
            });
        }
        function getApiDetails()
        {
            $(".search-bar .progress").fadeIn(300);
            var apiID = $("#searchField").val();
            if(apiID!="")
            {
                $.post(base_url+"assignments/getMotorApiDetail/"+apiID,function(data){
                    $(".search-bar .progress").fadeOut(300);
                    var motorData = jQuery.parseJSON(data);
                    var location = {lat: parseFloat(motorData.y), lng: parseFloat(motorData.x)};
                    map.setCenter(location);
                    locateMotor(motorData);
                    map.setZoom(15);
                });
            }

        }

        function locateMotor(motorData)
        {
            if(searchMarker != undefined)
            {
                searchMarker.setMap(null);
            }
            var marker = new google.maps.Marker({
                position: {lat: parseFloat(motorData.y), lng: parseFloat(motorData.x)},
                icon: icons.motor.icon,
                map: map,
                ogc_fid: motorData.ogc_fid
            });
            searchMarker = marker;
            addMotorInfoWindow(marker);
            marker.addListener('click', function() {
                getMotorData(marker.ogc_fid);
            });
        }

        function addMotorInfoWindow(motorMarker)
        {
            console.log("add motor info window");
            var ogc_fid = motorMarker.ogc_fid;
            $.post(base_url+"assignments/getMotorData/"+ogc_fid,function(data){
                data = jQuery.parseJSON(data);

                var title = data.label;
                var dataArray = {
                    "HP" : data.hp,
                    "PF" : data.pf,
                    "EFF" : data.eff,
                    "Motor Type" : data.motortype,
                    "Manufacturer" : data.manufacturer,
                    "Model Number" : data.modelnumber,
                    "Serial Number" : data.serialnumber,
                    "RPM" : data.rpm,
                    "Service Factor" : data.servicefactor,
                    "NEMA Type" : data.nematype,
                    "NEMA Design" : data.nemadesign,
                    "RTD Volts" : data.rtdvolts,
                    "Rated AMPs" : data.ratedamps,
                    "Encloser" : data.enclosure,
                    "API NO" : data.api_no,
                    "Link" : data.html_link,
                }
                var contentHtml = '<div class="object-title">'+title+'</div>';
                $.each(dataArray,function(attribute,value){
                    if(attribute!="Link")
                    {
                        if(value!=null)
                        {
                            contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span>'+value+'</div>';
                        }
                        
                    }
                    else
                    {
                        if (value.indexOf("http") >= 0)
                        {
                            contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span><a href="'+value+'" target="_blank">Click Here</a></div>';
                        }
                        
                    }
                    
                });
                var infowindow = new google.maps.InfoWindow({
                    content: contentHtml
                });
                infowindow.open(map, motorMarker);
                motorMarker.addListener('click', function() {
                    infowindow.open(map, motorMarker);
                });
            });  
        }

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 38.89511, lng: -77.03637},
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.HYBRID,
          mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_CENTER
          }
        });
        polyLine = new google.maps.Polyline({
            map: map,
            path: [],
            strokeColor: '#1E90FF',
            strokeOpacity: 1,
            strokeWeight: 3
        });
        screenResizeFix();
        plotAssignmentGeom();
        $(window).on('resize', function() {
            screenResizeFix();
        });
        //add a option to hide left sidebar
        $("#objectDetailFooter a").on("click",function(){
            $("#objectDetail").fadeOut(300);
        });

        $("#measureDistance").on("click",function(){
            if($("#measureDistance span").html()=="Clear/Deactivate")
            {
                $("#measureDistance span").html("Measure Distance");
                deactivateMeasureTool();
            }
            else
            {
                $("#measureDistance span").html("Clear/Deactivate");
                initMeasureTool();
            }
        });
      }

      function screenResizeFix()
      {
          $(document).height($(window).height());
          $("body").height($(window).height());
          $("#map").height($("body").height()-$("nav.darken-1").height()-$("nav.darken-3").height());
      }



      function plotAssignmentGeom()
      {
          showMapOverlay();
          var base_url = $("#base_url").val();
          var assignmentID = $("#assignmentID").val();
          $.post(base_url+"assignments/geomCheck/"+assignmentID,function(data){
              var jsonObject = jQuery.parseJSON(data);
              var assignmentData = jsonObject[0];
              wktToGeom(assignmentData.wkt);

              map.setCenter({lat: parseFloat(assignmentData.cent_y), lng: parseFloat(assignmentData.cent_x)});


              $.post(base_url+"assignments/getAssignmentRow/"+assignmentID,function(data){
                    var assignmentDetail = jQuery.parseJSON(data);
                    loadAssignmentDetails(assignmentDetail);
                    plotGeom(assignmentDetail.substation);
              });
              
          });
      }

      function loadAssignmentDetails(assignmentData)
      {
          var contentHtml = "<h4>"+assignmentData.assignmentid+"</h4>";
          var assignmentDetail = {
              "Type" : assignmentData.type,
              "Priority" : assignmentData.priority,
              "Description" : assignmentData.description,
              "Resolution" : assignmentData.resolution,
              "Assignment Date" : assignmentData.assigndate,
              "Expected Date" : assignmentData.expecteddate,
              "Crew Name" : assignmentData.crewname,
              "Substation" : assignmentData.name,
              "status" : assignmentData.status,
          }

          $.each(assignmentDetail,function(attribute,value){
              contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span>'+value+'</div>';
          });
          $("#assignmentDetails").html(contentHtml);
          $("#assignmentDetails").fadeIn(300);
      }


      function wktToGeom(wkt)
      {
            var geojson = $.geo.WKT.parse(wkt);
            var multiPolygonFeature = {
                "type": "Feature",
                "geometry": geojson,
                "properties": {
                    "strokeColor": '#1E90FF',
                    "strokeOpacity": 0.8,
                    "strokeWeight": 2,
                    "fillColor": '#1E90FF',
                    "fillOpacity": 0.35,
                    "type": "assignmentPolygon"
                }
            };

            var multiPolygonGeoJson = {
                    "type": "FeatureCollection",
                    "features": [multiPolygonFeature]
            };
            
            map.data.addGeoJson(multiPolygonGeoJson);

            map.data.setStyle(function(feature) {
                if(feature.getProperty('type') == "assignmentPolygon")
                {
                    var StyleOptions = {
                        strokeColor: feature.getProperty('strokeColor'),
                        strokeOpacity: feature.getProperty('strokeOpacity'),
                        strokeWeight: feature.getProperty('strokeWeight'),
                        fillColor: feature.getProperty('fillColor'),
                        fillOpacity: feature.getProperty('fillOpacity')
                    }
                }
                else
                {
                    var color = feature.getProperty('color');
                    var StyleOptions = {
                        fillColor: color,
                        strokeColor: color,
                        strokeWeight: 4
                    }
                }
               
                return /** @type {google.maps.Data.StyleOptions} */(StyleOptions);
            });
            /*
            var poly = new google.maps.Polygon({
                paths: ptsArray,
                strokeColor: '#1E90FF',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#1E90FF',
                fillOpacity: 0.35
            });
            */
            map.setZoom(15);
      }


      //function to add points from individual rings
      function AddPoints(data){
          //first spilt the string into individual points
          var pointsData=data.split(",");
          //iterate over each points data and create a latlong
          //& add it to the cords array
          var len=pointsData.length;
          for (var i=0;i<len;i++)
          {
              var xy=pointsData[i].split(" ");
              var pt= {lat: parseFloat(xy[1]), lng: parseFloat(xy[0])};
              ptsArray.push(pt);
          }
    }


    function getPoleData(ogc_fid)
    {
        $("#objectDetail").fadeIn(300);
        $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
        $.post(base_url+"assignments/getPoleData/"+ogc_fid,function(data){
            data = jQuery.parseJSON(data);
            showPoleData(data);
        });
    }

    function showPoleData(data)
    {
        var title = "Pole";
        var dataArray = {
            "Comment" : data.comments,
            "Pole Type" : data.poletype,
            "Substation" : data.s_substation,
            "Subsection" : data.s_subsection,
            "Datasource" : data.data_source,
            "Link" : data.html_link
        }
        updateObjectInfowindow(title,dataArray)
    }

    function getSubstationData(substationID)
    {
        $("#objectDetail").fadeIn(300);
        $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
        $.post(base_url+"assignments/getSubstationData/"+substationID,function(data){
            data = jQuery.parseJSON(data);
            showSubstationData(data);
        });
    }

    function showSubstationData(data)
    {
        var title = data.name;
        var dataArray = {
            "Comment" : data.comments,
            "utility" : data.utility,
            "Contract" : data.oxy_contact,
            "Owner" : data.owner,
            "Oxy Region" : data.oxyregion,
            "Link" : data.html_link,
        }
        updateObjectInfowindow(title,dataArray)
    }




    function initMeasureTool()
    {
        map.setOptions({draggableCursor:'crosshair'});
        google.maps.event.addListener(map, "click", function (event) {
            //event.preventDefault();
        
            var markerIndex = polyLine.getPath().length;
            polyLine.setMap(map);
            var isFirstMarker = markerIndex === 0;
            var measureMarker = {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: '#1E90FF',
                fillOpacity: 0.6,
                scale: 6,
                strokeColor: '#1565C0',
                strokeOpacity: 1,
                strokeWeight: 2
            };
            var marker = new google.maps.Marker({
                map: map,
                icon: measureMarker,
                position: event.latLng,
                draggable: true
            });
            
            if (isFirstMarker) {
                google.maps.event.addListener(marker, 'click', function () {
                    var path = polyLine.getPath();
                    polyGon.setPath(path);
                    polyGon.setMap(map);
                });
            }
            
            polyLine.getPath().push(event.latLng);        
            outlineMarkers.push(marker);
                    
            google.maps.event.addListener(marker, 'drag', function (dragEvent) {
                polyLine.getPath().setAt(markerIndex, dragEvent.latLng);
                updateDistance(outlineMarkers);
            });  
            updateDistance(outlineMarkers);
        }); 
        $("#measureDistanceDisplay").fadeIn(300);
    }
    function deactivateMeasureTool()
    {
        map.setOptions({draggableCursor:null});
        polyLine.setMap(null);
        google.maps.event.clearListeners(map, 'click');
        $.each(outlineMarkers,function(key,marker){
            marker.setMap(null);
        });
        outlineMarkers = [];
        polyLine = new google.maps.Polyline({
            map: map,
            path: [],
            strokeColor: '#1E90FF',
            strokeOpacity: 1,
            strokeWeight: 3
        });
        $("#measureDistanceDisplay span").html("0.0");
        $("#measureDistanceDisplay").fadeOut(300);
    }
    function updateDistance(outlineMarkers){
        var totalDistance = 0.0;
            var last = undefined;
            $.each(outlineMarkers, function(index, val){
                if(last){
                    totalDistance += google.maps.geometry.spherical.computeDistanceBetween(last.position, val.position);
                }
                last = val;
            });
            var distanceInKm = (totalDistance/1000).toFixed(2);
            var distanceInMeter = totalDistance.toFixed(2);
            var distanceInFeet = (distanceInMeter*3.280839895).toFixed(2);
            var distanceInMiles = (distanceInKm/1.60934).toFixed(2);
            if(distanceInFeet >= 2500)
            {
                var displayDistance = distanceInMiles+"miles";
            }
            else
            {
                var displayDistance = distanceInFeet+"ft";
            }
            $("#measureDistanceDisplay span").html(displayDistance);
    }