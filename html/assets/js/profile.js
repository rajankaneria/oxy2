/* profile page js */
$("#saveBtn").on("click",function(){
    var base_url = $("#base_url").val();
    var userData = {
        "first_name": $("#first_name").val(),
        "last_name": $("#last_name").val(),
        "phone": $("#phone").val()
    }
    var validationFlag = 1;
    $.each(userData,function(key,value){
        if(value=="" || value==null || value==undefined)
        {
            validationFlag = 0;
        }
    });

    if(validationFlag == 1)
    {
        $(".progress").fadeIn(300);
        $.post(base_url+"user/updateUser",{userData:userData},function(data){
            $(".progress").fadeOut(300);
            $("#userFullName").html(userData["first_name"]+" "+userData["last_name"]);
        });
    }
    else
    {
        $(".error-msg").fadeIn(300).delay(5000).fadeOut(300);
    }


});