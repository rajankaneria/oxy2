$(function(){
    $("#loginBtn").on("click",function(){
        var email = $("#loginEmail").val();
        var password = $("#loginPassword").val();
        $(".progress").fadeIn(300);
        $.post("login/loginCheck",{email:email,password:password},function(data){
            //console.log(data);
            if(data=="fail")
            {
                $(".progress").fadeOut(300);
                $('#loginMsg').openModal();
            }
            else
            {
                window.location.href = "dashboard";
            }   
        });
    });
});