
    $(function(){
        initMap();
        initDrawingTool();
        initMaterialDesign();
        initFormElements();
        initSearchBar();
        $('#createFieldContainer').slimScroll({
            height: '450px'
        });   
        $.post(base_url+"assignments/getAssignmentTypes",function(data){
            data = jQuery.parseJSON(data);
            var autocompleteData = {};
            $.each(data,function(key,typeData){
                autocompleteData[typeData.name] = null;
            });
            $('#assignmentType').autocomplete({
                data: autocompleteData
            });
        });

    });
    function initSearchBar()
    {
        $.post(base_url+"assignments/getMotorApiList",function(data){
            $(".search-bar").fadeIn(300);
            console.log("api_no loaded");
            data = jQuery.parseJSON(data);
            var motorData = {};
            $.each(data,function(key,apiData){
                motorData[apiData.api_no] = null;
            });
            $('#searchField').autocomplete({
                data: motorData
            });
            $('#searchField').keypress(function(e) {
                if(e.which == 13) {
                    getApiDetails();
                }
            });
        });
        $("#searchBtn").on("click",function(){
            getApiDetails();
        });
    }
    function getApiDetails()
    {
        $(".search-bar .progress").fadeIn(300);
        var apiID = $("#searchField").val();
        if(apiID!="")
        {
            $.post(base_url+"assignments/getMotorApiDetail/"+apiID,function(data){
                $(".search-bar .progress").fadeOut(300);
                var motorData = jQuery.parseJSON(data);
                var location = {lat: parseFloat(motorData.y), lng: parseFloat(motorData.x)};
                map.setCenter(location);
                locateMotor(motorData);
                map.setZoom(15);
            });
        }

    }

    function locateMotor(motorData)
    {
        if(searchMarker != undefined)
        {
            searchMarker.setMap(null);
        }
        var marker = new google.maps.Marker({
            position: {lat: parseFloat(motorData.y), lng: parseFloat(motorData.x)},
            icon: icons.motor.icon,
            map: map,
            ogc_fid: motorData.ogc_fid
        });
        searchMarker = marker;

        addMotorInfoWindow(marker);
        marker.addListener('click', function() {
            getMotorData(marker.ogc_fid);
        });
    }
    function addMotorInfoWindow(motorMarker)
    {
        var ogc_fid = motorMarker.ogc_fid;
        $.post(base_url+"assignments/getMotorData/"+ogc_fid,function(data){
            data = jQuery.parseJSON(data);

            var title = data.label;
            var dataArray = {
                "HP" : data.hp,
                "PF" : data.pf,
                "EFF" : data.eff,
                "Motor Type" : data.motortype,
                "Manufacturer" : data.manufacturer,
                "Model Number" : data.modelnumber,
                "Serial Number" : data.serialnumber,
                "RPM" : data.rpm,
                "Service Factor" : data.servicefactor,
                "NEMA Type" : data.nematype,
                "NEMA Design" : data.nemadesign,
                "RTD Volts" : data.rtdvolts,
                "Rated AMPs" : data.ratedamps,
                "Encloser" : data.enclosure,
                "API NO" : data.api_no,
                "Link" : data.html_link,
            }
            var contentHtml = '<div class="object-title">'+title+'</div>';
            $.each(dataArray,function(attribute,value){
                if(attribute!="Link")
                {
                    if(value!=null)
                    {
                        contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span>'+value+'</div>';
                    }
                    
                }
                else if(value!=null)
                {
                    if (value.indexOf("http") >= 0)
                    {
                        contentHtml = contentHtml+'<div class="object-row"><span class="object-attribute">'+attribute+': </span><a href="'+value+'" target="_blank">Click Here</a></div>';
                    }
                    
                }
                
            });
            var infowindow = new google.maps.InfoWindow({
                content: contentHtml
            });
            infowindow.open(map, motorMarker);
            motorMarker.addListener('click', function() {
                infowindow.open(map, motorMarker);
            });
        });


        
    }

    function initFormElements()
    {
        //on selecting a substation plot motor,poles and substation geometry on map and zoom to substation location
        $("#substation").on("change",function(){
            var substation = $("option:selected",this);
            var location = {lat: substation.data("lat"), lng: substation.data("lng")}
            var title = substation.text();
            var substationID = substation.val();
            showMapOverlay();
            getSubstationData(substationID);
            plotGeom(substationID);
            zoomToLocation(substationID,location,title);
        });

        //add a option to hide left sidebar
        $("#objectDetailFooter a").on("click",function(){
            $("#objectDetail").fadeOut(300);
        });


        $("#saveBtn").on("click",function(){

            var assignmentData = {
                assignmentid: $("#assignment_id").val(),
                expecteddate: $("#expected_date").val(),
                crew: $("#crew").val(),
                substation: $("#substation").val(),
                description: $("#description").val(),
                wkb_geometry: $("#polygonwkt").val(),
                cent_x: $("#cent_x").val(),
                cent_y: $("#cent_y").val(),
                type: $("#assignmentType").val(),
                priority: $("#priority").val(),
                resolution: $("#resolution").val()
            }

            var validationFlag = 1;
            $.each(assignmentData,function(key,value){
                if(value=="" || value==null || value==undefined)
                {
                    validationFlag = 0;
                }
            });

            if(validationFlag == 1)
            {
                $("#createAssignmentForm .progress").fadeIn(300);
                $.post(base_url+"assignments/saveAssignment",{assignmentData:assignmentData},function(data){
                    window.location.reload();
                });
            }
            else
            {
                $(".error-msg").fadeIn(300).delay(5000).fadeOut(300);
            }

            
        });

    }


    function getCentroid(path)
    {
        var bounds = new google.maps.LatLngBounds()
        path.forEach(function(element,index){bounds.extend(element)})
        var centroid = bounds.getCenter();
        console.log(centroid);
        return {lat:centroid.lat(),lng:centroid.lng()};
    }


    function initMaterialDesign()
    {
        $('select').material_select();
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
    }

    function activateDrawingTool()
    {
        drawingManager.setDrawingMode(google.maps.drawing.OverlayType.POLYGON);
        drawingManager.setMap(map);
        $("#deactivatePolygonBtn").removeAttr("disabled");
        $("#drawPolygonBtn").attr("disabled","disabled");
        $("#clearPolygonBtn").removeAttr("disabled");
    }
    function deactivateDrawingTool()
    {
        drawingManager.setMap(null);
        $("#drawPolygonBtn").removeAttr("disabled");
        $("#deactivatePolygonBtn").attr("disabled","disabled");
    }
    function initDrawingTool()
    {
        
        $("#clearPolygonBtn").on("click",function(){
            deactivateDrawingTool();
            $("#clearPolygonBtn").attr("disabled","disabled");
            $.each(assignmentPolygonArray,function(key,assignmentPolygon){
                assignmentPolygon.setMap(null); //clear previous graphics added by the drawing tool
            });
            assignmentPolygonArray = [];
            $("#polygonwkt").val("");
            $("#cent_x").val("");
            $("#cent_y").val("");
            polygonData = [];
        });
        //initialize polygon draw on button click
        $("#drawPolygonBtn").on("click",function(){
            activateDrawingTool();
        });

        $("#deactivatePolygonBtn").on("click",function(){
            deactivateDrawingTool();
        });

        //save polygon wkt to send it to database on polygon draw complete
        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
            assignmentPolygonArray.push(event.overlay);
            //assignmentPolygon = event.overlay;

            
            //convert overlay path to geojson format and then to wkt
            var feature = new google.maps.Data.Feature({geometry:new google.maps.Data.Polygon([event.overlay.getPath().getArray()])});
            feature.toGeoJson(function(json){ 
                var geom = json.geometry;
                polygonData.push(geom);
            });
            var multiPolygonWKT = createMultiPolygon(polygonData);
            $("#polygonwkt").val(multiPolygonWKT);
            //save centroid of the geometry
            var latLng = getCentroid(event.overlay.getPath());
            $("#cent_x").val(latLng.lng);
            $("#cent_y").val(latLng.lat);

            //Deactivate drawing tool
            //drawingManager.setMap(null);
        });

    }

    function createMultiPolygon(polygonGeomArray)
    {
        var multiPolygonCoordinates = [];
        $.each(polygonGeomArray,function(key,polygonGeom){
            multiPolygonCoordinates.push(polygonGeom.coordinates);
        });
        var multiPolygonGeom = {
            "type": "MultiPolygon",
            "coordinates": multiPolygonCoordinates
        }
        var wkt = $.geo.WKT.stringify(multiPolygonGeom);
        return wkt;
    }

    function initMeasureTool()
    {
        map.setOptions({draggableCursor:'crosshair'});
        google.maps.event.addListener(map, "click", function (event) {
            //event.preventDefault();
        
            var markerIndex = polyLine.getPath().length;
            polyLine.setMap(map);
            var isFirstMarker = markerIndex === 0;
            var measureMarker = {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: '#1E90FF',
                fillOpacity: 0.6,
                scale: 6,
                strokeColor: '#1565C0',
                strokeOpacity: 1,
                strokeWeight: 2
            };
            var marker = new google.maps.Marker({
                map: map,
                icon: measureMarker,
                position: event.latLng,
                draggable: true
            });
            
            if (isFirstMarker) {
                google.maps.event.addListener(marker, 'click', function () {
                    var path = polyLine.getPath();
                    polyGon.setPath(path);
                    polyGon.setMap(map);
                });
            }
            
            polyLine.getPath().push(event.latLng);        
            outlineMarkers.push(marker);
                    
            google.maps.event.addListener(marker, 'drag', function (dragEvent) {
                polyLine.getPath().setAt(markerIndex, dragEvent.latLng);
                updateDistance(outlineMarkers);
            });  
            updateDistance(outlineMarkers);
        }); 
        $("#measureDistanceDisplay").fadeIn(300);
    }
    function deactivateMeasureTool()
    {
        map.setOptions({draggableCursor:null});
        polyLine.setMap(null);
        google.maps.event.clearListeners(map, 'click');
        $.each(outlineMarkers,function(key,marker){
            marker.setMap(null);
        });
        outlineMarkers = [];
        polyLine = new google.maps.Polyline({
            map: map,
            path: [],
            strokeColor: '#1E90FF',
            strokeOpacity: 1,
            strokeWeight: 3
        });
        $("#measureDistanceDisplay span").html("0.0");
        $("#measureDistanceDisplay").fadeOut(300);
    }
    function updateDistance(outlineMarkers){
        var totalDistance = 0.0;
            var last = undefined;
            $.each(outlineMarkers, function(index, val){
                if(last){
                    totalDistance += google.maps.geometry.spherical.computeDistanceBetween(last.position, val.position);
                }
                last = val;
            });
            var distanceInKm = (totalDistance/1000).toFixed(2);
            var distanceInMeter = totalDistance.toFixed(2);
            var distanceInFeet = (distanceInMeter*3.280839895).toFixed(2);
            var distanceInMiles = (distanceInKm/1.60934).toFixed(2);
            if(distanceInFeet >= 2500)
            {
                var displayDistance = distanceInMiles+"miles";
            }
            else
            {
                var displayDistance = distanceInFeet+"ft";
            }
            $("#measureDistanceDisplay span").html(displayDistance);
    }
    function initMap() {
        //Initialize map
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 38.89511, lng: -77.03637},
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            mapTypeControlOptions: {
              style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
              position: google.maps.ControlPosition.TOP_CENTER
          }
        });
        screenResizeFix(); // resize window to remove scroll bar
        
        //recalculating screen size when size of window changes
        $(window).on('resize', function() {
            screenResizeFix();
        });
        polyLine = new google.maps.Polyline({
            map: map,
            path: [],
            strokeColor: '#1E90FF',
            strokeOpacity: 1,
            strokeWeight: 3
        });
        drawingManager = new google.maps.drawing.DrawingManager({                
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: false,
            polygonOptions: {
                strokeColor: '#1E90FF',
                strokeOpacity: 1,
                strokeWeight: 3,
                fillColor: '#1E90FF',
                fillOpacity: 0.6,
                zIndex: 100
            }
        });
        drawingManager.setMap(null);
        $("#measureDistance").on("click",function(){
            if($("#measureDistance span").html()=="Clear/Deactivate")
            {
                $("#measureDistance span").html("Measure Distance");
                deactivateMeasureTool();
            }
            else
            {
                $("#measureDistance span").html("Clear/Deactivate");
                initMeasureTool();
            }
        });
        /*
        var legend = document.getElementById('legend');
        for (var key in icons) {
            var type = icons[key];
            var name = type.name;
            var icon = type.icon;
            var div = document.createElement('div');
            div.innerHTML = '<div class="row legend-row"><div class="legend-icon"><img src="' + icon + '" style="width:20px;"></div><div class="legend-name">'+name+'</div></div> ';
            legend.appendChild(div);
        }

        map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(legend);
        */

    }

    function screenResizeFix()
    {
          $(document).height($(window).height());
          $("body").height($(window).height());
          $("#map").height($("body").height()-$("nav.darken-1").height()-$("nav.darken-3").height());
    }



    function getPoleData(ogc_fid)
    {
        $("#objectDetail").fadeIn(300);
        $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
        $.post(base_url+"assignments/getPoleData/"+ogc_fid,function(data){
            data = jQuery.parseJSON(data);
            showPoleData(data);
        });
    }

    function showPoleData(data)
    {
        var title = "Pole";
        var dataArray = {
            "Comment" : data.comments,
            "Pole Type" : data.poletype,
            "Substation" : data.s_substation,
            "Subsection" : data.s_subsection,
            "Datasource" : data.data_source,
            "Link" : data.html_link
        }
        updateObjectInfowindow(title,dataArray)
    }

    function getSubstationData(substationID)
    {
        $("#objectDetail").fadeIn(300);
        $("#objectDetail #mainContent").html('<div class="object-title">Loading...</div>');
        $.post(base_url+"assignments/getSubstationData/"+substationID,function(data){
            data = jQuery.parseJSON(data);
            showSubstationData(data);
        });
    }

    function showSubstationData(data)
    {
        var title = data.name;
        var dataArray = {
            "Comment" : data.comments,
            "utility" : data.utility,
            "Contract" : data.oxy_contact,
            "Owner" : data.owner,
            "Oxy Region" : data.oxyregion,
            "Link" : data.html_link,
        }
        updateObjectInfowindow(title,dataArray)
    }



    


    function zoomToLocation(substationID,location,title)
    {
        map.setCenter(location);
        addMarker(substationID,location,title);
        map.setZoom(15);
    }

    function addMarker(substationID,location, title) {
        clearMarkers();
        var marker = new google.maps.Marker({
            position: location,
            icon: icons.substation.icon,
            map: map,
            title: title,
            substationID: substationID
        });

        marker.addListener('click', function() {
            getSubstationData(marker.substationID);
        });
        markers.push(marker);
    }

    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    function clearMarkers() {
        setMapOnAll(null);
    }

    function geocodeLocation(location)
    {
        geocoder.geocode({'location': location}, function(results, status) { 
            if (status === 'OK') 
            {
                return results[1].formatted_address;
            }
            else
            {
                return "Could not retrive address!";
            }
        });

    }