$(function(){
	var api_no = $("#api_no").val();
	var base_url = $("#base_url").val();
    $(".map-overlay").fadeIn(300);
	$.post(base_url+"mobile/getMotorEdges/"+api_no,function(data){
		var canvasPoints = $.parseJSON(data);
        var nodeDataSet = [];
        var edgeDataSet = [];
        var nodeIds = [];
        var DIR = base_url+"html/assets/icons/motorDiagram/";
        $.each(canvasPoints.nodeList,function(dataIndex,dataObject){
                var currentNode = {
                    id: dataIndex,
                    label: dataObject.label,
                    image: DIR + dataObject.tableName+'.png', 
                    shape: 'image'
                };
            nodeDataSet.push(currentNode);
        });
        var lineColorArray = {
            "ohprimary" : {
                color: 'orange',
                width: 2,
                dashes:false
            },
            "ohsecondary" : {
                color: 'red',
                width: 2,
                dashes:false
            },
            "ugsecondary" : {
                color: 'orange',
                width: 2,
                dashes:true
            },
            "ugprimary" : {
                color: 'red',
                width: 2,
                dashes:true
            }
        };
        $.each(canvasPoints.lines,function(dataIndex,dataObject){
            $.each(dataObject,function(lineIndex,lineArray){
                edgeDataSet.push({from: lineArray["fromguid"], to: lineArray["toguid"],lineType:dataIndex, color: lineColorArray[dataIndex]["color"], width: lineColorArray[dataIndex]["width"], dashes: lineColorArray[dataIndex]["dashes"]});
            });
        });
        networkEdgeArray = edgeDataSet;
        var container = document.getElementById('motorDiagram');
        var nodes = new vis.DataSet(nodeDataSet);
        var edges = new vis.DataSet(edgeDataSet);
        var data = {
            nodes: nodes,
            edges: edges
        };
        var options = {
            nodes: {
            	scaling: {
                	min: 16,
                	max: 32
              	},
              	font:{
                	color: "#000",
                	size: 16
              	}
            },
            edges: {
              	color: 'grey',
              	smooth: false,
              	hoverWidth: 3
            },
            physics:{
              	barnesHut:{gravitationalConstant:-30000},
              	stabilization: {iterations:2500}
            },
            interaction:{
                selectConnectedEdges: false,
                hover: true
            }
        };
		network = new vis.Network(container, data, options);
        $(".map-overlay").fadeOut(300);

        network.on('selectNode', function(p) {
            var selectedNodeId = p.nodes[0];
            var selectedElement = canvasPoints.nodeList[selectedNodeId].label;
            $(".map-overlay").fadeIn(300);
            $.post(base_url+"mobile/getFeatureDetails/",{featureName:selectedElement,featureId:selectedNodeId},function(data){
                data = $.parseJSON(data);
                $("#featureDetailModal #detailContainer").html("");
                $("#featureDetailModal #detailContainer").append("<div class='motor-feature-title'>"+selectedElement+"</div>");
                $.each(data.fields,function(key,fieldRow){
                    var fieldLabel = fieldRow["field_label"];
                    var fieldName = fieldRow["field_name"];
                    var isLink = fieldRow["is_link"];

                    
                    if(isLink==0)
                    {
                        $("#featureDetailModal #detailContainer").append("<b>"+fieldLabel+"</b>: ");
                        $("#featureDetailModal #detailContainer").append(data.data[fieldName]);
                    }
                    else
                    {
                        $("#featureDetailModal #detailContainer").prepend("<a class='btn-full waves-effect waves-light btn blue darken-2' href='"+data.data[fieldName]+"' target='_blank'>Click to see Pictures</a>");
                    }
                    
                    $("#featureDetailModal #detailContainer").append("<br/>");
                });
                $(".map-overlay").fadeOut(300);
                $("#featureDetailModal").openModal();
            });
        });

        network.on('selectEdge',function(q){
            var selectedEdgeId = q.edges[0];
            var selectedEdgeData = networkEdgeArray.filter(function ( obj ) {
                return obj.id === selectedEdgeId;
            })[0];
            $(".map-overlay").fadeIn(300);
            $.post(base_url+"mobile/getLineDetails/",{lineType:selectedEdgeData.lineType,from:selectedEdgeData.from,to:selectedEdgeData.to},function(data){
                data = $.parseJSON(data);
                $("#featureDetailModal #detailContainer").html("");
                $("#featureDetailModal #detailContainer").append("<div class='motor-feature-title'>"+selectedEdgeData.lineType+"</div>");
                $.each(data.fields,function(key,fieldRow){
                    var fieldLabel = fieldRow["field_label"];
                    var fieldName = fieldRow["field_name"];
                    var isLink = fieldRow["is_link"];
                    
                    if(isLink==0)
                    {
                        $("#featureDetailModal #detailContainer").append("<b>"+fieldLabel+"</b>: ");
                        $("#featureDetailModal #detailContainer").append(data.data[fieldName]);
                    }
                    else
                    {
                        $("#featureDetailModal #detailContainer").prepend("<a class='btn-full waves-effect waves-light btn blue darken-2' href='"+data.data[fieldName]+"' target='_blank'>Click to see Pictures</a>");
                    }
                    
                    $("#featureDetailModal #detailContainer").append("<br/>");
                });
                $(".map-overlay").fadeOut(300);
                $("#featureDetailModal").openModal();
            });
        });


	});
});