
$(function(){
    $("#submitChat").on("click",function(){
        sendMsg();
    });
    $('#chat_msg').on('keyup', function(e) {
        if (e.keyCode === 13) {
            sendMsg();
        }
    });

    $('.modal-trigger').leanModal();
    
    $("#chatContainer").slimscroll();
    
    $("#openChat").on("click",function(){
        var assignmentID;
        if($("#grid .k-state-selected").length != 0)
        {
            var grid = $("#grid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            assignmentID = selectedItem.id;
        }
        if(assignmentID == undefined)
        {
            $('#assignmentMsg').openModal();
        }
        else
        {
            $('#chatModal').openModal();
            loadAssignmentChat();
        }
        
    });


});


function loadAssignmentChat()
{
    var base_url = $("#base_url").val();
    var grid = $("#grid").data("kendoGrid");
    var selectedItem = grid.dataItem(grid.select());
    var assignmentID = selectedItem.id;
    var assignmentTitle = selectedItem.assignmentid;
    var selectedRow = grid.select();
    var selectedRowIndex = selectedRow.index();
    var uid = selectedItem.uid;
    
    $('#grid').data("kendoGrid").dataSource.data()[selectedRowIndex].newChatCount = 0;
    $('#grid').data("kendoGrid").dataSource.sync();
    $('#grid').data('kendoGrid').refresh();
    selectGridRow(assignmentID);
    
    $("#chatModal h4").html(assignmentTitle);
    $("#chatContainer").html("Loading Chats...");
    $.post(base_url+"chat/getChat/"+assignmentID+"/",function(data){
        data = jQuery.parseJSON(data);
        var chatHtml = data.chatHtml;
        $("#chatContainer").html(chatHtml);
        updateScrollBar();
        $("#chat_msg").val("");
    });

}

function selectGridRow(assignmentID)
{
    var gridData = $('#grid').data("kendoGrid").dataSource.data();
    $.each(gridData,function(key,gridRow){
        if (gridRow.id == assignmentID) { 
            var row =  $('#grid').data("kendoGrid").table.find("[data-uid=" + gridRow.uid + "]");
            $('#grid').data("kendoGrid").select(row);
            return false; 
        }
    });
}

function sendMsg()
{
    var grid = $("#grid").data("kendoGrid");
    var selectedItem = grid.dataItem(grid.select());
    var assignmentID = selectedItem.id;
    var base_url = $("#base_url").val();
    var chatMsg = $("#chat_msg").val();
    $("#chat_msg").attr("disabled","disabled");
    $("#submitChat").addClass("disabled");
    $.post(base_url+"chat/sendMsg/",{msg:chatMsg,assignmentID:assignmentID},function(data){
        data = jQuery.parseJSON(data);
        var chatHtml = data.chatHtml;
        $("#chatContainer").append(chatHtml);
        updateScrollBar();
        $("#chat_msg").removeAttr("disabled");
        $("#submitChat").removeClass("disabled");
        $("#chat_msg").val("");
        Materialize.updateTextFields();
    });
}

function updateScrollBar()
{
    $('#chatContainer').slimScroll({ scrollTo: $("#chatContainer")[0].scrollHeight+'px' });
}