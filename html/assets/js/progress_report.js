var kendoGridData;
var savedFields = {};
$(function(){
    initKendoGrid();



    $("#loadFields").on("click",function(){

        var selectedSubstations = [];
        if($("#grid .k-state-selected").length == 0)
        {
            $('#reportMsg').openModal();
        }
        else
        {
            var grid = $("#grid").data("kendoGrid");
            grid.select().each(function() {
                var dataItem = grid.dataItem($(this));
                selectedSubstations.push(dataItem);
            });
            $('#fieldListModal').openModal();
            /*
            var selectedData = $.grep(kendoGridData, function(v,i) {
                return v["idsubstations"] === selectedItem.fid;
            });
            */
            initSelectedSubstationFields(selectedSubstations);
            console.log(selectedSubstations);    
        }
    });


    $(".field-save-btn").on("click",function(){
        saveField();
    });

    $("#sendNotification").on("click",function(){
        sendNotification();
    });

});

function sendNotification()
{
    $("#sendNotification").html("Processing...");
    var base_url = $("#base_url").val();
    //console.log(savedFields);
    $.post(base_url+"user/sendReportNotification/",{savedFields:savedFields},function(data){
        $("#sendNotification").html("Done");
        $('#fieldListModal').closeModal();
    });
}

function initSelectedSubstationFields(substationList)
{
    savedFields = {};
    $("#fieldListGrid").html("");
    $("#fieldGridOverlay").hide();
    var base_url = $("#base_url").val();
    $.post(base_url+"user/getCurrentUser/",function(data){

        var userRow = $.parseJSON(data);
        //console.log(userRow);
        var fieldList = ["field_collection","engineering_analysis","arcflash","gis","delivery","acceptance"];
        var backgroundArray = {};
        $.each(fieldList,function(key,fieldName){
            backgroundArray[fieldName] = "";
            if(userRow[fieldName] == 1)
            {
                backgroundArray[fieldName] = " background: darkseagreen;"
            }
        });

        $(".overlay-close-btn").off("click");
        $(".overlay-close-btn").on("click",function(){
            $("#fieldGridOverlay").fadeOut(300);
        });
            Materialize.updateTextFields();
            $("#fieldListGrid").kendoGrid({
                scrollable:true,
                resizable: true,
                selectable: "cell",
                columns: [{
                    field: "name",
                    title: "Name",
                    width: 200,
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "field_collection_status",
                    title: "Field collection status",
                    attributes: {
                        style: "text-align: center;"+backgroundArray["field_collection"]
                    },
                    width: 200

                },{
                    field: "field_collection_comments",
                    title: "Field collection comments",
                    attributes: {
                        style: backgroundArray["field_collection"]
                    },
                    width: 200
                },{
                    field: "engineering_analysis_status",
                    title: "Engineering analysis status",
                    attributes: {
                        style: "text-align: center;"+backgroundArray["engineering_analysis"]
                    },
                    width: 200
                },{
                    field: "engineering_analysis_comments",
                    title: "Engineering analysis comments",
                    attributes: {
                        style: backgroundArray["engineering_analysis"]
                    },
                    width: 200
                },{
                    field: "arcflash_status",
                    title: "Arcflash status",
                    attributes: {
                        style: "text-align: center;"+backgroundArray["arcflash"]
                    },
                    width: 200
                },{
                    field: "arcflash_comments",
                    title: "Arcflash comments",
                    attributes: {
                        style: backgroundArray["arcflash"]
                    },
                    width: 200
                },{
                    field: "gis_status",
                    title: "GIS status",
                    attributes: {
                        style: "text-align: center;"+backgroundArray["gis"]
                    },
                    width: 200
                },{
                    field: "gis_comments",
                    title: "GIS comments",
                    attributes: {
                        style: backgroundArray["gis"]
                    },
                    width: 200
                },{
                    field: "delivery_status",
                    title: "Delivery status",
                    attributes: {
                        style: "text-align: center;"+backgroundArray["delivery"]
                    },
                    width: 200
                },{
                    field: "delivery_comments",
                    title: "Delivery comments",
                    attributes: {
                        style: backgroundArray["delivery"]
                    },
                    width: 200
                },{
                    field: "acceptance_status",
                    title: "Acceptance status",
                    attributes: {
                        style: "text-align: center;"+backgroundArray["acceptance"]
                    },
                    width: 200
                },{
                    field: "acceptance_comments",
                    title: "Acceptance comments",
                    attributes: {
                        style: backgroundArray["acceptance"]
                    },
                    width: 200
                }],
                dataSource:substationList,
                pageable: false,
                change: function() {
                    var cell = this.select(),
                        cellIndex = cell.index(),
                        column = this.columns[cellIndex],
                        dataSource = this.dataSource,
                        dataItem = dataSource.view()[cell.closest("tr").index()];
                            
                    console.log(column.field);
                    var fieldTypeArray = column.field.split("_");
                    var fieldType = fieldTypeArray[fieldTypeArray.length - 1];
                    var fieldName = fieldTypeArray.slice(0, fieldTypeArray.length - 1).join("_");
                    //console.log(fieldName);
                    //console.log(userRow[fieldName]);
                    if(userRow[fieldName] == 1)
                    {
                        if(fieldType == "comments")
                        {
                            //$('#fieldCommentModal').openModal();
                            $("#fieldComment").val(dataItem[column.field]);
                            $(".field-input").hide();
                            $("#commentContainer").show();
                            $("#fieldGridOverlay").fadeIn(300);
                            Materialize.updateTextFields();
                        }
                        else if(fieldType == "status")
                        {
                            $(".field-status-select").hide();
                            if(column.field=="accptance_status")
                            {
                                $("#accptanceFieldStatus").show();
                            }
                            else
                            {
                                $("#fieldStatus").show();
                            }
                            $("#fieldStatus").val(dataItem[column.field]);
                            $(".field-input").hide();
                            $("#statusContainer").show();
                            $("#fieldGridOverlay").fadeIn(300);
                            Materialize.updateTextFields();
                        }

                    }
                    //console.log(dataItem.idsubstations);
                }
            });

    });

}

function saveField()
{
    var grid = $("#fieldListGrid").data("kendoGrid");
    var cell = grid.select(),
    cellIndex = cell.index(),
    column = grid.columns[cellIndex],
    dataSource = grid.dataSource,
    dataItem = dataSource.view()[cell.closest("tr").index()];

    var fieldTypeArray = column.field.split("_");
    var fieldType = fieldTypeArray[fieldTypeArray.length - 1];
    var fieldName = fieldTypeArray.slice(0, fieldTypeArray.length - 1).join("_");

    if(fieldType == "comments")
    {
        var newValue = $("#fieldComment").val();
        $("#saveComment").html("Saving...");
    }
    else if(fieldType == "status")
    {
        var newValue = $("#fieldStatus option:selected").val();
        if(column.field=="accptance_status")
        {
            newValue = $("#accptanceFieldStatus option:selected").val();
        }
        $("#saveStatus").html("Saving...");
    }
    if(savedFields["substation_"+dataItem.idsubstations] == undefined){ savedFields["substation_"+dataItem.idsubstations] = []; }
    savedFields["substation_"+dataItem.idsubstations].push({substationID:dataItem.idsubstations,column:column.field,value:newValue,substationName:dataItem.name});

    var base_url = $("#base_url").val();
    $.post(base_url+"database/saveSubstationField/",{substationID:dataItem.idsubstations,column:column.field,value:newValue},function(data){
        dataItem.set(column.field, newValue);
        $("#fieldGridOverlay").fadeOut(300);
        $(".field-save-btn").html("Save");
    });

}


function initKendoGrid()
{
    $("#selectAllGridRow").off("click");
    $("#selectAllGridRow").on("click",function(){
        $("tbody tr").addClass("k-state-selected");
        $("tbody tr input.checkbox").prop('checked', true);
    });
    var base_url = $("#base_url").val();
    $.post(base_url+"database/getSubstationList/",function(data){
        data = jQuery.parseJSON(data);
        kendoGridData = data;
        console.log(data);
            $("#grid").kendoGrid({
                scrollable: false,
                sortable: true,
                selectable: "multiple, row",
                columns: [{
                    field: "idsubstations",
                    title: "Substation ID",
                    attributes: {
                        style: "text-align: center;"
                    },
                    template: '<input class="checkbox filled-in" type="checkbox" id="#: idsubstations #" /><label class="checkbox-label" for="#: idsubstations #">#: idsubstations #</label>'
                },{
                    field: "name",
                    title: "Name",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "oxyregion",
                    title: "Region",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "comments",
                    title: "Comments"
                }],
                dataSource:data,
                pageable:{
                    pageSize: 100,
                },
                change: function(e) {
                  $('tr').find('[type=checkbox]').prop('checked', false);
                  $('tr.k-state-selected').find('[type=checkbox]').prop('checked', true);
                },
                dataBound: function () {
                    $(".checkbox-label").on("click",function(e){
                        e.preventDefault();
                    });
                    $(".checkbox").bind("change", function (e) {
                        console.log(e);
                        $(e.target).closest("tr").toggleClass("k-state-selected");
                    });
                    
                }
            });
            $("#gridTools").show();


            $("#gridSearch").keyup(function () {
                var val = $('#gridSearch').val();
                $("#grid").data("kendoGrid").dataSource.filter({
                    logic  : "or",
                    filters: [
                        {
                            field   : "name",
                            operator: "contains",
                            value   : val
                        }
                    ]
                });
            });


            /*
            var tdCounter = 1;
            $(".k-grid-header-wrap th").each(function(){
                $(this).width($(".k-grid-content tr td:nth-child("+tdCounter+")").width());
                tdCounter++;
            });
            */
    });
}