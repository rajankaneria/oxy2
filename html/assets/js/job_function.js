var kendoGridData;
$(function(){
    initKendoGrid();
    $("#updateJobFunction").on("click",function(){
        openJobFunctionModal();
    });
    $("#saveJobfunctionBtn").on("click",function(){
        saveJobFunction();
    });
});

function openJobFunctionModal()
{
        if($("#grid .k-state-selected").length != 0)
        {
            var grid = $("#grid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            jobFunction = selectedItem.s_job_function_id;
            console.log(selectedItem);
        }
        if(jobFunction == undefined)
        {
            $('#userMsg').openModal();
        }
        else
        {
            $("#jobFunctionModal").openModal();
            $("#jobFunction").val(selectedItem.s_job_function_id);
        }
}

function saveJobFunction()
{
    var grid = $("#grid").data("kendoGrid");
    var selectedItem = grid.dataItem(grid.select());
    var newJobFunction = $("#jobFunction option:selected").val();
    var base_url = $("#base_url").val();
    $("#jobFunctionModal .progress").fadeIn(300);
    $.post(base_url+"user/saveJobFunction/",{user:selectedItem.id,jobfunction:newJobFunction},function(data){
        initKendoGrid();
    });
}

function initKendoGrid()
{
    var base_url = $("#base_url").val();
    $.post(base_url+"user/getUserList/",function(data){
        data = jQuery.parseJSON(data);
        kendoGridData = data;
        console.log(data);
            $("#grid").kendoGrid({
                scrollable: false,
			    sortable: true,
                selectable: true,
                columns: [{
                    field: "id",
                    title: "ID",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "first_name",
                    title: "First Name",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "last_name",
                    title: "Last Name",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "email",
                    title: "Email",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "phone",
                    title: "Phone",
                    attributes: {
                        style: "text-align: center;"
                    }
                },{
                    field: "s_job_function_id",
                    title: "Job Function",
                    attributes: {
                        style: "text-align: center;"
                    }
                }],
                dataSource:data
            });

            /*
            var tdCounter = 1;
            $(".k-grid-header-wrap th").each(function(){
                $(this).width($(".k-grid-content tr td:nth-child("+tdCounter+")").width());
                tdCounter++;
            });
            */
            $("#jobFunctionModal .progress").hide();
            $("#jobFunctionModal").closeModal();
    });
}