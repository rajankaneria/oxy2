/* js for signup page */


$(function(){
    $("#signupBtn").on("click",function(){
        signup();
    });
});

function signup()
{
    var base_url = $("#base_url").val();
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var phone = $("#phone").val();

    var userData = {
        "first_name":first_name,
        "last_name":last_name,
        "email":email,
        "password":password,
        "phone":phone
    }
    var validationFlag = 1;
    $.each(userData,function(key,value){
        if(value=="" || value==null || value==undefined)
        {
            validationFlag = 0;
        }
    });
    if(validationFlag == 1)
    {
        $(".progress").fadeIn(300);
        $.post(base_url+"signup/register",{userData:userData},function(data){
            $(".progress").fadeOut(300);
            if(data=="fail")
            {
                $('#signupErrorMsg').openModal();
            }
            else
            {
                $('#signupSuccessMsg').openModal();
                $("input[type=text]").val("");
                $("input[type=email]").val("");
                $("input[type=password]").val("");
            }   
        });
    }
    else
    {
        $(".error-msg").fadeIn(300).delay(5000).fadeOut(300);
    }


}